﻿using UnityEngine;

namespace MazeGenerator
{
    //Cardinal Orientation
    using CO = Navigation.CO;

    /// <summary>Handles keyboard inputs from the user.</summary>
    public class InputHandler : MonoBehaviour
    {
        /// <summary>Indicates if the character is occupied in an animation.</summary>
        private bool occupied;
        /// <summary>Indicates if the UI occupied in an animation.</summary>
        private bool UIOccupied;

        /// <summary>Start is called before the first frame update (MonoBehaviour method). Initializes the occupied boolean to <c>false</c>.</summary>
        private void Start()
        {
            occupied = false;
            UIOccupied = false;
        }

        /// <summary>Update is called at each frame (MonoBehavious method). Handles input if character is unoccupied.</summary>
        void Update()
        {
            HandlePlayerInput();
        }

        /// <summary>Toggles the occupied boolean.</summary>
        public void ToggleOccupied()
        {
            occupied = !occupied;
        }

        /// <summary>Toggles the UI occupied boolean.</summary>
        public void ToggleUIOccupied()
        {
            UIOccupied = !UIOccupied;
        }

        /// <summary>
        /// Handles player input. Input invokes different movement actions depending on which camera is on display.
        /// </summary>
        private void HandlePlayerInput()
        {
            int activeCamera = CameraBehaviour.GetInstance().activeCamera;
            CO orientation = gameObject.GetComponent<Player>().Orientation;
            if (!occupied && (Input.GetKey(ControlConstants.CONTROLS_UP1) || Input.GetKey(ControlConstants.CONTROLS_UP2)))
            {
                if (activeCamera == 0)
                {
                    PlayerMovement.HandlePlayerMovement(CO.North);
                }
                else
                {
                    PlayerMovement.HandlePlayerMovement(orientation);
                }
            }
            else if (!occupied && (Input.GetKey(ControlConstants.CONTROLS_DOWN1) || Input.GetKey(ControlConstants.CONTROLS_DOWN2)))
            {
                if (activeCamera == 0)
                {
                    PlayerMovement.HandlePlayerMovement(CO.South);
                }
                else
                {
                    PlayerMovement.HandlePlayerMovement(Navigation.OppositeCO(orientation));
                }
            }
            else if (!occupied && (Input.GetKey(ControlConstants.CONTROLS_LEFT1) || Input.GetKey(ControlConstants.CONTROLS_LEFT2)))
            {
                if (activeCamera == 0)
                {
                    PlayerMovement.HandlePlayerMovement(CO.West);
                }
                else
                {
                    PlayerMovement.HandlePlayerMovement(Navigation.NextCO(orientation, false));
                }
            }
            else if (!occupied && (Input.GetKey(ControlConstants.CONTROLS_RIGHT1) || Input.GetKey(ControlConstants.CONTROLS_RIGHT2)))
            {
                if (activeCamera == 0)
                {
                    PlayerMovement.HandlePlayerMovement(CO.East);
                }
                else
                {
                    PlayerMovement.HandlePlayerMovement(Navigation.NextCO(orientation, true));
                }
            }
            else if (!UIOccupied && Input.GetKey(ControlConstants.CONTROLS_HIDEUI))
            {
                UIHandler.GetInstance().ToggleUI();
            }
            else if (!UIOccupied && Input.GetKey(ControlConstants.CONTROLS_CAMERA))
            {
                CameraBehaviour.GetInstance().ChangeCamera();
            }
        }
    }
}