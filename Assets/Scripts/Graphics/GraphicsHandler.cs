﻿using System.Collections.Generic;
using UnityEngine;

namespace MazeGenerator
{
    /// <summary>
    /// The GraphicsHandler class handles all graphical updates that happen in the scene. It is
    /// responsible for drawing and updating the maze (cells, walls, pillars), character and orbs.
    /// </summary>
    public class GraphicsHandler : MonoBehaviour
    {
        /// <summary>The instance of this class.</summary>
        private static GraphicsHandler instance;
        /// <summary>A random number generator.</summary>
        private static System.Random rnd = new System.Random();

        /// <summary>A HashSet containing all graphical GameObjects.</summary>
        private HashSet<GameObject> graphicsObjects;
        /// <summary>A Dictionary that maps a wall to its GameObject. Does not include outer maze walls.</summary>
        private Dictionary<Wall, GameObject> wallToGO;
        /// <summary>A Dictionary that maps a cell to its GameObject.</summary>
        private Dictionary<Cell, GameObject> cellToGO;

        /// <summary>The starting X position (bottom left corner) of wall GameObjects.</summary>
        private float wallStartX;
        /// <summary>The starting Z position (bottom left corner) of wall GameObjects.</summary>
        private float wallStartZ;
        /// <summary>The starting X position (bottom left corner) of pillar GameObjects.</summary>
        private float pillarStartX;
        /// <summary>The starting Z position (bottom left corner) of pillar GameObjects.</summary>
        private float pillarStartZ;
        /// <summary>The starting X position (bottom left corner) of cell GameObjects.</summary>
        private float cellStartX;
        /// <summary>The starting Z position (bottom left corner) of cell GameObjects.</summary>
        private float cellStartZ;

        /// <summary>Start is called before the first frame update (MonoBehaviour method). Initializes the graphics fields.</summary>
        void Start()
        {
            InitializeGraphicsFields();
        }

        /// <summary>Gets the only instance of this class.</summary>
        /// <returns>GraphicsHandler instance.</returns>
        public static GraphicsHandler GetInstance()
        {
            return instance;
        }

        /// <summary>Returns all current walls in the maze.</summary>
        /// <returns>HashSet of all walls in the maze.</returns>
        public HashSet<Wall> GetWalls()
        {
            HashSet<Wall> walls = new HashSet<Wall>();
            foreach (KeyValuePair<Wall, GameObject> keyValuePair in wallToGO)
            {
                walls.Add(keyValuePair.Key);
            }
            return walls;
        }

        /// <summary>Removes a graphics GameObject from the scene.</summary>
        /// <param name="GO">The GameObject to remove.</param>
        public void RemoveGraphicsObject(GameObject GO)
        {
            if (graphicsObjects.Contains(GO)) graphicsObjects.Remove(GO);
            Destroy(GO);
        }

        /// <summary>Initializes the graphics fields.</summary>
        private void InitializeGraphicsFields()
        {
            instance = this;
            graphicsObjects = new HashSet<GameObject>();
            wallToGO = new Dictionary<Wall, GameObject>();
            cellToGO = new Dictionary<Cell, GameObject>();
            cellStartX = cellStartZ = pillarStartX = pillarStartZ  = wallStartX = wallStartZ = 0f;
        }

        /// <summary>
        /// Initializes the graphics. This includes:
        /// <list type="bullet">
        /// <item> Resetting the graphics (removing all GameObjects from the scene). </item>
        /// <item> Instantiating cells. </item>
        /// <item> Instantiating pillars. </item>
        /// <item> Instantiating walls. </item>
        /// </list>
        /// </summary>
        public void InitializeGraphics()
        {
            ResetGraphics();
            InstantiateCells();
            InstantiatePillars();
            InstantiateWalls();
            InstantiateGates();
        }

        /// <summary>Resets the graphics (removes all GameObjects from the scene).</summary>
        private void ResetGraphics()
        {
            foreach (GameObject go in graphicsObjects)
            {
                Destroy(go);
            }
            graphicsObjects.Clear();
            wallToGO.Clear();
            cellToGO.Clear();
        }

        /// <summary>Instantiates all maze cell GameObjects (the floor).</summary>
        private void InstantiateCells()
        {
            float offsetX = (MazeGenerator.GetInstance().width % 2 == 0) ? GraphicsConstants.CELL_HALF_SIZE : GraphicsConstants.CELL_SIZE;
            float offsetZ = (MazeGenerator.GetInstance().height % 2 == 0) ? GraphicsConstants.CELL_HALF_SIZE : GraphicsConstants.CELL_SIZE;

            cellStartX = offsetX - (MazeGenerator.GetInstance().width * GraphicsConstants.CELL_HALF_SIZE);
            cellStartZ = offsetZ - (MazeGenerator.GetInstance().height * GraphicsConstants.CELL_HALF_SIZE);

            for (int x = 0; x < MazeGenerator.GetInstance().width; x++)
            {
                for (int z = 0; z < MazeGenerator.GetInstance().height; z++)
                {
                    Cell cell = new Cell(x, z);
                    InstantiateCell(cell);
                }
            }

            //Instantiate victory cell (behind exit gate)
            Cell victoryCell = new Cell(0, MazeGenerator.GetInstance().height);
            GameObject cellGO = Instantiate(Resources.Load(PrefabConstants.PREFAB_CELL), new Vector3(cellStartX + victoryCell.X, GraphicsConstants.CELL_ELEVATION, cellStartZ + victoryCell.Z), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
            graphicsObjects.Add(cellGO);
        }

        /// <summary>Instantiates all maze pillar GameObjects.</summary>
        private void InstantiatePillars()
        {
            float offsetX = (MazeGenerator.GetInstance().width % 2 == 0) ? 0f : GraphicsConstants.CELL_HALF_SIZE;
            float offsetZ = (MazeGenerator.GetInstance().height % 2 == 0) ? 0f : GraphicsConstants.CELL_HALF_SIZE;

            pillarStartX = offsetX - (MazeGenerator.GetInstance().width * GraphicsConstants.CELL_HALF_SIZE);
            pillarStartZ = offsetZ - (MazeGenerator.GetInstance().height * GraphicsConstants.CELL_HALF_SIZE);

            for (int x = 0; x <= MazeGenerator.GetInstance().width; x++)
            {
                for (int z = 0; z <= MazeGenerator.GetInstance().height; z++)
                {
                    InstantiatePillar(x, z);
                }
            }
        }

        /// <summary>Instantiates all maze wall GameObjects.</summary>
        private void InstantiateWalls()
        {
            float offsetX = (MazeGenerator.GetInstance().width % 2 == 0) ? GraphicsConstants.CELL_HALF_SIZE : GraphicsConstants.CELL_SIZE;
            float offsetZ = (MazeGenerator.GetInstance().height % 2 == 0) ? 0f : GraphicsConstants.CELL_HALF_SIZE;

            wallStartX = offsetX - (MazeGenerator.GetInstance().width * 0.5f * GraphicsConstants.CELL_SIZE);
            wallStartZ = offsetZ - (MazeGenerator.GetInstance().height * 0.5f * GraphicsConstants.CELL_SIZE);

            InstantiateOuterWalls();
            //Recursive Division is the only algorithm that starts with no inner walls.
            if (!(MazeGenerator.GetInstance().mazeGenerationAlgorithm is MazeGenerationAlgorithm_RecursiveDivision))
            {
                InstantiateInnerWalls();
            }
        }

        /// <summary>Instantiates the gate GameObjects.</summary>
        private void InstantiateGates()
        {
            GameObject entranceGateGO = Instantiate(Resources.Load(PrefabConstants.PREFAB_GATE), new Vector3(wallStartX + MazeGenerator.GetInstance().width - 1, GraphicsConstants.GATE_ELEVATION, wallStartZ), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
            GameObject exitGateGO = Instantiate(Resources.Load(PrefabConstants.PREFAB_GATE), new Vector3(wallStartX + 0, GraphicsConstants.GATE_ELEVATION, wallStartZ + MazeGenerator.GetInstance().height), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
            graphicsObjects.Add(entranceGateGO);
            graphicsObjects.Add(exitGateGO);

            GameplayHandler.GetInstance().exitGateGO = exitGateGO;
        }

        /// <summary>Instantiates a single cell GameObject.</summary>
        /// <param name="cell">The cell we want to instantiate as a GameObject.</param>
        private void InstantiateCell(Cell cell)
        {
            GameObject cellGO = Instantiate(Resources.Load(PrefabConstants.PREFAB_CELL), new Vector3(cellStartX + cell.X, GraphicsConstants.CELL_ELEVATION, cellStartZ + cell.Z), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
            cellToGO.Add(cell, cellGO);
            graphicsObjects.Add(cellGO);
            InitializeCellColor(cell);
        }

        /// <summary>Instantiates a single pillar GameObject.</summary>
        /// <param name="x">The x position of the pillar.</param>
        /// <param name="z">The z position of the pillar.</param>
        private void InstantiatePillar(int x, int z)
        {
            GameObject pillarGO = Instantiate(Resources.Load(PrefabConstants.PREFAB_PILLAR), new Vector3(pillarStartX + x, GraphicsConstants.PILLAR_ELEVATION, pillarStartZ + z), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
            graphicsObjects.Add(pillarGO);
        }

        /// <summary>Instantiates the outer walls of the maze.</summary>
        private void InstantiateOuterWalls()
        {
            //Bottom line of walls (leave right-most one open, this is the maze entrance)
            for (int x = 0; x < MazeGenerator.GetInstance().width - 1; x++)
            {
                GameObject wallGO = Instantiate(Resources.Load(PrefabConstants.PREFAB_WALL), new Vector3(wallStartX + x, GraphicsConstants.WALL_ELEVATION, wallStartZ), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                graphicsObjects.Add(wallGO);
            }

            //Top line of walls (leave left-most one open, this is the maze exit)
            for (int x = 1; x < MazeGenerator.GetInstance().width; x++)
            {
                GameObject wallGO = Instantiate(Resources.Load(PrefabConstants.PREFAB_WALL), new Vector3(wallStartX + x, GraphicsConstants.WALL_ELEVATION, wallStartZ + MazeGenerator.GetInstance().height), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                graphicsObjects.Add(wallGO);
            }

            //Left and right lines of walls
            for (int z = 0; z < MazeGenerator.GetInstance().height; z++)
            {
                GameObject wallGO1 = Instantiate(Resources.Load(PrefabConstants.PREFAB_WALL), new Vector3(wallStartX - GraphicsConstants.CELL_HALF_SIZE, GraphicsConstants.WALL_ELEVATION, wallStartZ + z + GraphicsConstants.CELL_HALF_SIZE), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                GameObject wallGO2 = Instantiate(Resources.Load(PrefabConstants.PREFAB_WALL), new Vector3(wallStartX + MazeGenerator.GetInstance().width - GraphicsConstants.CELL_HALF_SIZE, GraphicsConstants.WALL_ELEVATION, wallStartZ + z + GraphicsConstants.CELL_HALF_SIZE), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                graphicsObjects.Add(wallGO1);
                graphicsObjects.Add(wallGO2);
            }
        }

        /// <summary>Instantiates the inner walls of the maze.</summary>
        private void InstantiateInnerWalls()
        {
            InstantiateHorizontalInnerWalls();
            InstantiateVerticalInnerWalls();
        }

        /// <summary>Instantiates the horizontal inner walls of the maze.</summary>
        private void InstantiateHorizontalInnerWalls()
        {
            for (int x = 0; x < MazeGenerator.GetInstance().width; x++)
            {
                for (int z = 1; z < MazeGenerator.GetInstance().height; z++)
                {
                    Cell bottomCell = new Cell(x, z - 1);
                    Cell topCell = new Cell(x, z);
                    Wall wall = new Wall(bottomCell, topCell);
                    InstantiateHorizontalWall(wall);
                }
            }
        }

        /// <summary>Instantiates the vertical inner walls of the maze.</summary>
        private void InstantiateVerticalInnerWalls()
        {
            for (int x = 1; x < MazeGenerator.GetInstance().width; x++)
            {
                for (int z = 0; z < MazeGenerator.GetInstance().height; z++)
                {
                    Cell bottomCell = new Cell(x - 1, z);
                    Cell topCell = new Cell(x, z);
                    Wall wall = new Wall(bottomCell, topCell);
                    InstantiateVerticalWall(wall);
                }
            }
        }

        /// <summary>Instantiates a single horizontal wall GameObject.</summary>
        /// <param name="wall">The wall that needs to be instantiated as a GameObject.</param>
        private void InstantiateHorizontalWall(Wall wall)
        {
            int x = wall.Cell1.X;
            int maxZ = (wall.Cell1.Z > wall.Cell2.Z) ? wall.Cell1.Z : wall.Cell2.Z;
            GameObject wallGO = Instantiate(Resources.Load(PrefabConstants.PREFAB_WALL), new Vector3(wallStartX + x, GraphicsConstants.WALL_ELEVATION, wallStartZ + maxZ), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
            wallToGO.Add(wall, wallGO);
            graphicsObjects.Add(wallGO);
        }

        /// <summary>Instantiates a single vertical wall GameObject.</summary>
        /// <param name="wall">The wall that needs to be instantiated as a GameObject.</param>
        private void InstantiateVerticalWall(Wall wall)
        {
            int z = wall.Cell1.Z;
            int maxX = (wall.Cell1.X > wall.Cell2.X) ? wall.Cell1.X : wall.Cell2.X;
            GameObject wallGO = Instantiate(Resources.Load(PrefabConstants.PREFAB_WALL), new Vector3(wallStartX + maxX - GraphicsConstants.CELL_HALF_SIZE, GraphicsConstants.WALL_ELEVATION, wallStartZ + z + GraphicsConstants.CELL_HALF_SIZE), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
            wallToGO.Add(wall, wallGO);
            graphicsObjects.Add(wallGO);
        }

        /// <summary>Initializes the color of the maze cells depending on the algorithm used.</summary>
        /// <param name="cell">The cell we need to color.</param>
        private void InitializeCellColor(Cell cell)
        {
            // Kruskal and Recursive Division deal with walls, so we color all cells as part of the maze from the beginning.
            if (MazeGenerator.GetInstance().mazeGenerationAlgorithm is MazeGenerationAlgorithm_Kruskal
                || MazeGenerator.GetInstance().mazeGenerationAlgorithm is MazeGenerationAlgorithm_RecursiveDivision)
            {
                ColorCell(cell, CellType.Maze);
            }
            else
            {
                ColorCell(cell, CellType.Unexplored);
            }
        }

        /// <summary>Updates the maze graphics from a MazeUpdate object (includes cell and wall updates).</summary>
        /// <param name="mazeUpdate">An object that includes all cell and wall updates.</param>
        public void UpdateMaze(MazeUpdate mazeUpdate)
        {
            UpdateWalls(mazeUpdate.wallUpdates);
            UpdateCells(mazeUpdate.cellUpdates);
        }

        /// <summary>Updates maze walls from a list of wall updates.</summary>
        /// <param name="wallUpdates">The list of walls that need to be updated.
        /// Each update holds a wall and bool that indicates if it should be removed or created.</param>
        private void UpdateWalls(List<WallUpdate> wallUpdates)
        {
            foreach (WallUpdate wallUpdate in wallUpdates)
            {
                if (wallUpdate.removeWall) RemoveWall(wallUpdate.wall);
                else CreateWall(wallUpdate.wall);
            }
        }

        /// <summary>Updates maze cells from a list of cell updates.</summary>
        /// <param name="cellUpdates">The list of cells that need to be updated.
        /// Each update holds a cell and its cell type.</param>
        private void UpdateCells(List<CellUpdate> cellUpdates)
        {
            foreach (CellUpdate cellUpdate in cellUpdates)
            {
                ColorCell(cellUpdate.cell, cellUpdate.cellType);
            }
        }

        /// <summary>Removes a wall from the maze.</summary>
        /// <param name="wall">The wall we want to remove.</param>
        private void RemoveWall(Wall wall)
        {
            GameObject wallGO;
            if (wallToGO.TryGetValue(wall, out wallGO))
            {
                wallToGO.Remove(wall);
                graphicsObjects.Remove(wallGO);
                Destroy(wallGO);
            }
            else
            {
                Debug.LogError("No such key:\n" + wall);
            }
        }

        /// <summary>Creates a new wall in the maze.</summary>
        /// <param name="wall">The wall we want to create as a GameObject.</param>
        private void CreateWall(Wall wall)
        {
            if (wall.Cell1.X == wall.Cell2.X) InstantiateHorizontalWall(wall);
            else InstantiateVerticalWall(wall);
        }

        /// <summary>Colors a cell in the maze based on its type.</summary>
        /// <param name="cell">The cell that needs to be colored.</param>
        /// <param name="cellType">The type of the cell that needs to be colored.</param>
        private void ColorCell(Cell cell, CellType cellType)
        {
            GameObject cellGO = cellToGO[cell];
            Color color = GraphicsConstants.UNEXPLORED_CELL_COLOR;
            switch (cellType)
            {
                case CellType.Maze:
                    color = GraphicsConstants.MAZE_CELL_COLOR;
                    break;
                case CellType.Explored:
                    color = GraphicsConstants.EXPLORED_CELL_COLOR;
                    break;
                case CellType.Unexplored:
                    color = GraphicsConstants.UNEXPLORED_CELL_COLOR;
                    break;
                case CellType.Lead:
                    color = GraphicsConstants.LEAD_CELL_COLOR;
                    break;
            }
            cellGO.GetComponent<Renderer>().material.color = color;
        }

        /// <summary>Instantiates the playable character.</summary>
        public void InstantiateCharacter()
        {
            GameObject playerGO = Instantiate(Resources.Load(PrefabConstants.PREFAB_CHARACTER), new Vector3(cellStartX + MazeGenerator.GetInstance().width - 1, GraphicsConstants.CHARACTER_ELEVATION, cellStartZ), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
            float characterScale = GraphicsConstants.CHARACTER_SCALE;
            playerGO.transform.localScale = new Vector3(characterScale, characterScale, characterScale);
            graphicsObjects.Add(playerGO);
            GameplayHandler.GetInstance().playerGO = playerGO;
        }

        /// <summary>Instantiates orbs in the maze.</summary>
        /// <param name="numberOfOrbs">The number of orbs to instantiate.</param>
        public void InstantiateOrbs(int numberOfOrbs)
        {
            HashSet<Cell> occupiedCells = new HashSet<Cell>();
            //The only occupied cell at this point is the cell under the player (starting position)
            occupiedCells.Add(new Cell(MazeGenerator.GetInstance().width - 1, 0));
            for (int i = 0; i < numberOfOrbs; i++)
            {
                //Get a random free cell to instantiate an orb on
                Cell randomFreeCell = GetRandomFreeCell(ref occupiedCells);
                GameObject orbGO = Instantiate(Resources.Load(PrefabConstants.PREFAB_ORB), new Vector3(cellStartX + randomFreeCell.X, GraphicsConstants.ORB_ELEVATION, cellStartZ + randomFreeCell.Z), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                graphicsObjects.Add(orbGO);
            }
        }

        /// <summary>Gets a random free cell (that has no orb or character on it).</summary>
        /// <param name="occupiedCells">Reference to the set of occupied cells.</param>
        private Cell GetRandomFreeCell(ref HashSet<Cell> occupiedCells)
        {
            bool found = false;
            Cell result = new Cell(0, 0);
            while(!found)
            {
                int randomX = rnd.Next(MazeGenerator.GetInstance().width);
                int randomZ = rnd.Next(MazeGenerator.GetInstance().height);
                Cell randomCell = new Cell(randomX, randomZ);
                if (!occupiedCells.Contains(randomCell))
                {
                    occupiedCells.Add(randomCell);
                    result = randomCell;
                    found = true;
                }
            }
            return result;
        }
    }
}