﻿using System.Collections.Generic;

namespace MazeGenerator
{
    /// <summary>
    /// A static class that holds the Estension Method for shuffling a list
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>Random number generator.</summary>
        private static System.Random rnd = new System.Random();

        /// <summary>Shuffles a list randomly.</summary>
        /// <param name="list">The list.</param>
        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}