﻿using UnityEngine;

namespace MazeGenerator
{
    /// <summary>
    /// The Navigation class includes different methods for navigation in a maze, such as:
    /// <list type="number">
    /// <item> Obtaining the opposite orientation. </item>
    /// <item> Obtaining the next (clockwise/anticlockwise) orientation. </item>
    /// <item> Obtaining the next cell in a certain direction. </item>
    /// <item> Checking if a cell is within maze bounds. </item>
    /// <item> Converting an orientation to a direction vector. </item>
    /// <item> Calculating the rotation value to go from one orientation to another. </item>
    /// </list>
    /// </summary>
    public static class Navigation
    {
        /// <summary>Cardinal Orientation.</summary>
        public enum CO
        {
            /// <summary>North.</summary>
            North,
            /// <summary>East.</summary>
            East,
            /// <summary>South.</summary>
            South,
            /// <summary>West.</summary>
            West,
            /// <summary>Unknown.</summary>
            Unknown
        }

        /// <summary>Gets the opposite orientation.</summary>
        /// <param name="co">The current orientation.</param>
        /// <returns>The opposite orientation.</returns>
        public static CO OppositeCO(CO co)
        {
            switch (co)
            {
                case CO.North: return CO.South;
                case CO.South: return CO.North;
                case CO.West: return CO.East;
                case CO.East: return CO.West;
            }
            return CO.Unknown;
        }

        /// <summary>Gets the next orientation, depending if clockwise or anticlockwise.</summary>
        /// <param name="co">The current orientation.</param>
        /// <param name="clockWise"><c>true</c> for clockwise.</param>
        /// <returns>The next orientation.</returns>
        public static CO NextCO(CO co, bool clockWise)
        {
            if (clockWise) return NextCO_Clockwise(co);
            else return NextCO_Anticlockwise(co);
        }

        /// <summary>Gets the next clockwise orientation.</summary>
        /// <param name="co">The current orientation.</param>
        /// <returns>The next clockwise orientation.</returns>
        public static CO NextCO_Clockwise(CO co)
        {
            switch (co)
            {
                case CO.North: return CO.East;
                case CO.East: return CO.South;
                case CO.South: return CO.West;
                case CO.West: return CO.North;
            }
            return CO.Unknown;
        }

        /// <summary>Gets the next anticlockwise orientation.</summary>
        /// <param name="co">The current orientation.</param>
        /// <returns>The next anticlockwise orientation.</returns>
        public static CO NextCO_Anticlockwise(CO co)
        {
            switch (co)
            {
                case CO.North: return CO.West;
                case CO.West: return CO.South;
                case CO.South: return CO.East;
                case CO.East: return CO.North;
            }
            return CO.Unknown;
        }

        /// <summary>Gets the next cell in a certain direction.</summary>
        /// <param name="position">The current position (Cell).</param>
        /// <param name="direction">The desired direction.</param>
        /// <returns>The next cell in the desired direction.</returns>
        public static Cell NextCell(Cell position, CO direction)
        {
            Cell nextCell = position;
            switch (direction)
            {
                case CO.North:
                    nextCell += new Cell(0, 1);
                    break;
                case CO.South:
                    nextCell += new Cell(0, -1);
                    break;
                case CO.West:
                    nextCell += new Cell(-1, 0);
                    break;
                case CO.East:
                    nextCell += new Cell(1, 0);
                    break;
            }
            return nextCell;
        }

        /// <summary>Checks if a cell is within the maze bounds.</summary>
        /// <param name="cell">The cell.</param>
        /// <returns><c>true</c> if the cell is within bounds; otherwise, <c>false</c>.</returns>
        public static bool CellWithinBounds(Cell cell)
        {
            return (cell.X >= 0 && cell.X < MazeGenerator.GetInstance().width
                && cell.Z >= 0 && cell.Z < MazeGenerator.GetInstance().height);
        }

        /// <summary>Converts an orientation to a direction vector.</summary>
        /// <param name="direction">The desired orientation/direction.</param>
        /// <returns>The direction vector.</returns>
        public static Vector3 OrientationToVector(CO direction)
        {
            Vector3 directionVector = new Vector3(0, 0, 0);
            switch (direction)
            {
                case CO.North:
                    directionVector = new Vector3(0, 0, 1f);
                    break;
                case CO.South:
                    directionVector = new Vector3(0, 0, -1f);
                    break;
                case CO.West:
                    directionVector = new Vector3(-1f, 0, 0);
                    break;
                case CO.East:
                    directionVector = new Vector3(1f, 0, 0);
                    break;
            }
            return directionVector;
        }

        /// <summary>Calculates the rotation angle needed to rotate from one direction to the other.</summary>
        /// <param name="fromDirection">The starting orientation/direction.</param>
        /// <param name="toDirection">The end orientation/direction.</param>
        /// <returns>The rotation angle.</returns>
        public static float OrientationToRotation(CO fromDirection, CO toDirection)
        {
            float rotationAngle = 0f;
            switch (toDirection)
            {
                case CO.North:
                    rotationAngle = -90f;
                    if (fromDirection == CO.West) rotationAngle *= -1;
                    break;
                case CO.South:
                    rotationAngle = 90f;
                    if (fromDirection == CO.West) rotationAngle *= -1;
                    break;
                case CO.West:
                    rotationAngle = 90f;
                    if (fromDirection == CO.North) rotationAngle *= -1;
                    break;
                case CO.East:
                    rotationAngle = -90f;
                    if (fromDirection == CO.North) rotationAngle *= -1;
                    break;
            }
            return rotationAngle;
        }
    }
}