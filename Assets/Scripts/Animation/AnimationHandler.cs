﻿using UnityEngine;

namespace MazeGenerator
{
    //Cardinal Orientation
    using CO = Navigation.CO;

    /// <summary>
    /// The AnimationHandler class handles all animations. LeanTween is used for most animations.
    /// </summary>
    public static class AnimationHandler
    {
        /// <summary>Animates the player towards a certain direction.</summary>
        /// <param name="playerGO">The player GameObject.</param>
        /// <param name="direction">The direction the player should move towards.</param>
        public static void MovePlayerTowards(GameObject playerGO, CO direction)
        {
            float time = AnimationConstants.PLAYER_ANIMATION_TIME;
            Vector3 targetPosition = playerGO.transform.position + Navigation.OrientationToVector(direction);
            LeanTween.moveLocal(playerGO, targetPosition, time).setOnComplete(playerGO.GetComponent<InputHandler>().ToggleOccupied);
        }

        /// <summary>Animates the rotation of the player towards a certain direction.</summary>
        /// <param name="playerGO">The player GameObject.</param>
        /// <param name="direction">The direction the player should rotate towards.</param>
        public static void RotatePlayerTowards(GameObject playerGO, CO direction)
        {
            float time = AnimationConstants.PLAYER_ANIMATION_TIME;
            playerGO.GetComponent<InputHandler>().ToggleOccupied();
            CO orientation = playerGO.GetComponent<Player>().Orientation;
            float rotation = Navigation.OrientationToRotation(orientation, direction);
            LeanTween.rotateAroundLocal(playerGO, Vector3.up, rotation, time).setOnComplete(playerGO.GetComponent<InputHandler>().ToggleOccupied);
        }

        /// <summary>Animates an orb upwards.</summary>
        /// <param name="orbGO">The orb GameObject.</param>
        public static void MoveOrbUp(GameObject orbGO)
        {
            float time = AnimationConstants.ORB_ANIMATION_TIME;
            float ceiling = GraphicsConstants.ORB_ELEVATION + AnimationConstants.ORB_ANIMATION_DISTANCE;
            LeanTween.moveLocalY(orbGO, ceiling, time).setOnComplete(orbGO.GetComponent<Orb>().MoveDown);

        }

        /// <summary>Animates an orb downwards.</summary>
        /// <param name="orbGO">The orb GameObject.</param>
        public static void MoveOrbDown(GameObject orbGO)
        {
            float time = AnimationConstants.ORB_ANIMATION_TIME;
            float floor = GraphicsConstants.ORB_ELEVATION - AnimationConstants.ORB_ANIMATION_DISTANCE;
            LeanTween.moveLocalY(orbGO, floor, time).setOnComplete(orbGO.GetComponent<Orb>().MoveUp);

        }

        /// <summary>Animates the exit gate downwards.</summary>
        /// <param name="gateGO">The gate GameObject.</param>
        public static void OpenGate(GameObject gateGO)
        {
            float time = AnimationConstants.GATE_ANIMATION_TIME;
            float endPosition = AnimationConstants.GATE_ANIMATION_END_POSITION;
            LeanTween.moveLocalY(gateGO, endPosition, time).setOnComplete(GameplayHandler.GetInstance().AllowPassageOutOfMaze);
        }

        /// <summary>Animates the orb progress bar by scaling its x value.</summary>
        /// <param name="bar">The bar GameObject.</param>
        /// <param name="scale">The scaling value (percentage of completion).</param>
        public static void ScaleBar(GameObject bar, float scale)
        {
            float time = AnimationConstants.BAR_ANIMATION_TIME;
            LeanTween.scaleX(bar, scale, time);
        }

        /// <summary>Animates character victory animation.</summary>
        /// <param name="playerGO">The player GameObject.</param>
        public static void PlayVictoryAnimation(GameObject playerGO)
        {
            AnimationClip animClip = AnimationConstants.ANIMATION_VICTORY;
            animClip.legacy = true;
            animClip.wrapMode = WrapMode.Loop;
            playerGO.GetComponent<Player>().playerAnim.enabled = true;
            playerGO.GetComponent<Player>().playerAnim.Play(animClip.name);
        }
    }
}