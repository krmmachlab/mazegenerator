﻿namespace MazeGenerator
{
    /// <summary>Represents a wall in the maze. It is defined by two cells which it separates.</summary>
    public class Wall
    {
        /// <summary>The first cell.</summary>
        private Cell cell1;
        /// <summary>The second cell.</summary>
        private Cell cell2;

        /// <summary>
        /// Initializes a new instance of <see cref="Wall"/> class.
        /// </summary>
        public Wall(Cell _cell1, Cell _cell2)
        {
            cell1 = _cell1;
            cell2 = _cell2;
            SortCells(); //Make sure cell1 is less than cell2 (just a standard for equality)
        }

        /// <summary>Gets cell1 of this wall.</summary>
        public Cell Cell1
        {
            get { return cell1; }
        }

        /// <summary>Gets cell2 of this wall.</summary>
        public Cell Cell2
        {
            get { return cell2; }
        }

        /// <summary>Sorts the cells representing this wall so that the first one is the 'smaller' one.</summary>
        private void SortCells()
        {
            if (cell1.X > cell2.X)
            {
                SwapCells();
            }
            else
            {
                if (cell1.Z > cell2.Z)
                {
                    SwapCells();
                }
            }
        }

        /// <summary>Swaps cell1 and cell2.</summary>
        private void SwapCells()
        {
            Cell temp = cell1;
            cell1 = cell2;
            cell2 = temp;
        }

        /// <summary>Overrides the Equals method to check equality of 2 walls.</summary>
        /// <param name="obj">The object we want to check equality against.</param>
        /// <returns><c>true</c> if the 2 objects are equal; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            Wall wall = obj as Wall;
            return !ReferenceEquals(wall, null) && Equals(wall);
        }

        /// <summary>Checks equality of this wall with the passed wall.</summary>
        /// <param name="wall">The wall we want to check equality against.</param>
        /// <returns><c>true</c> if the 2 walls are equal; otherwise, <c>false</c>.</returns>
        public bool Equals(Wall wall)
        {
            return (Cell.Equals(cell1, wall.cell1) && Cell.Equals(cell2, wall.cell2));
        }

        /// <summary>Overrides the GetHashCode method.</summary>
        /// <returns>The hashcode of this object.</returns>
        public override int GetHashCode()
        {
            return new { cell1, cell2 }.GetHashCode();
        }

        /// <summary>Overrides the ToString method.</summary>
        /// <returns>A string representation of this wall.</returns>
        public override string ToString()
        {
            return cell1 + "-" + cell2;
        }
    }
}