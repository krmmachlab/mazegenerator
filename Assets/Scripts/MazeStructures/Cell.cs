﻿using System.Collections.Generic;

namespace MazeGenerator
{
    /// <summary>Represents a cell in the maze. It is defined by an x and z value.</summary>
    public class Cell
    {
        /// <summary>Cell x value.</summary>
        private int x;
        /// <summary>Cell z value.</summary>
        private int z;

        /// <summary>Gets the x value of this cell.</summary>
        public int X
        {
            get { return x; }
        }

        /// <summary>Gets the z value of this cell.</summary>
        public int Z
        {
            get { return z; }
        }

        /// <summary>
        /// Initializes a new instance of <see cref="Cell"/> class.
        /// </summary>
        public Cell(int _x, int _z)
        {
            x = _x;
            z = _z;
        }

        /// <summary>Calculates the neighbors of this cell. Returns a list of all neighbor cells.</summary>
        /// <returns>List of all neighbor cells.</returns>
        public List<Cell> CalculateNeighbors()
        {
            List<Cell> neighbors = new List<Cell>();
            if (x > 0)
            {
                Cell neighbor = new Cell(x - 1, z);
                neighbors.Add(neighbor);
            }
            if (x < MazeGenerator.GetInstance().width - 1)
            {
                Cell neighbor = new Cell(x + 1, z);
                neighbors.Add(neighbor);
            }
            if (z > 0)
            {
                Cell neighbor = new Cell(x, z - 1);
                neighbors.Add(neighbor);
            }
            if (z < MazeGenerator.GetInstance().height - 1)
            {
                Cell neighbor = new Cell(x, z + 1);
                neighbors.Add(neighbor);
            }
            return neighbors;
        }

        /// <summary>
        /// Calculates the walls of this cell. Returns a list of all surrounding walls (excluding outer maze walls).
        /// </summary>
        /// <returns>List of all neighbor cells.</returns>
        public List<Wall> CalculateWalls()
        {
            List<Wall> walls = new List<Wall>();
            foreach (Cell neighbor in CalculateNeighbors())
            {
                Wall wall = new Wall(this, neighbor);
                walls.Add(wall);
            }
            return walls;
        }

        /// <summary>Overrides the Equals method to check equality of 2 cells.</summary>
        /// <param name="obj">The object we want to check equality against.</param>
        /// <returns><c>true</c> if the 2 objects are equal; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            Cell cell = obj as Cell;
            return !ReferenceEquals(cell, null) && Equals(cell);
        }

        /// <summary>Checks equality of this cell with the passed cell.</summary>
        /// <param name="cell">The cell we want to check equality against.</param>
        /// <returns><c>true</c> if the 2 cell are equal; otherwise, <c>false</c>.</returns>
        public bool Equals(Cell cell)
        {
            return int.Equals(X, cell.X) && int.Equals(Z, cell.Z);
        }

        /// <summary>Overrides the GetHashCode method.</summary>
        /// <returns>The hashcode of this object.</returns>
        public override int GetHashCode()
        {
            return new { X, Z }.GetHashCode();
        }

        /// <summary>Overrides the ToString method.</summary>
        /// <returns>A string representation of this cell.</returns>
        public override string ToString()
        {
            return "(" + X + "," + Z + ")";
        }

        /// <summary>Overrides the + operator.</summary>
        /// <param name="cell1">First cell.</param>
        /// <param name="cell2">Second cell.</param>
        /// <returns>The sum of the two cells.</returns>
        public static Cell operator +(Cell cell1, Cell cell2)
        {
            return new Cell(cell1.X + cell2.X, cell1.Z + cell2.Z);
        }

        /// <summary>Overrides the - operator.</summary>
        /// <param name="cell1">First cell.</param>
        /// <param name="cell2">Second cell.</param>
        /// <returns>The difference of the two cells.</returns>
        public static Cell operator -(Cell cell1, Cell cell2)
        {
            return new Cell(cell1.X - cell2.X, cell1.Z - cell2.Z);
        }
    }
}