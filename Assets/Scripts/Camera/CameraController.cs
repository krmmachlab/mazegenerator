﻿using UnityEngine;

namespace MazeGenerator
{
    /// <summary>
    /// The CameraController class stores and controls camera information.
    /// </summary>
    public class CameraController : MonoBehaviour
    {
        /// <summary>The CameraController instance.</summary>
        private static CameraController instance;

        /// <summary>Third person camera.</summary>
        public Camera TP;
        /// <summary>First person camera.</summary>
        public Camera FP;
        /// <summary>Self person camera.</summary>
        public Camera SP;

        /// <summary>Start is called before the first frame update (MonoBehaviour method). Initializes the cameras.</summary>
        void Start()
        {
            instance = this;
            TP.targetDisplay = -1;
            FP.targetDisplay = -1;
            SP.targetDisplay = -1;
            TP.enabled = false;
            FP.enabled = false;
            SP.enabled = false;
        }

        /// <summary>Returns the instance of this class.</summary>
        public static CameraController GetInstance()
        {
            return instance;
        }
    }
}
