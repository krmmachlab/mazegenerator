﻿using System.Collections;
using UnityEngine;

namespace MazeGenerator
{
    /// <summary>
    /// The CameraBehaviour class handles camera centering and changing the displayed camera.
    /// </summary>
    public class CameraBehaviour : MonoBehaviour
    {
        /// <summary>The main camera in the scene.</summary>
        public Camera mainCamera;
        /// <summary>The camera audio source.</summary>
        private AudioSource cameraAudioSource;
        /// <summary>The current active camera.</summary>
        public int activeCamera;
        /// <summary>The CameraBehaviour instance.</summary>
        public static CameraBehaviour instance;

        /// <summary>Start is called before the first frame update (MonoBehaviour method). Initializes the camera.</summary>
        void Start()
        {
            instance = this;
            InitializeCamera();
        }

        /// <summary>Returns the instance of this class.</summary>
        public static CameraBehaviour GetInstance()
        {
            return instance;
        }

        /// <summary>Initializes the camera and adds an audio source to it that plays background music.</summary>
        private void InitializeCamera()
        {
            activeCamera = 0;
            mainCamera = Camera.main;
            mainCamera.orthographic = true;
            mainCamera.transform.Rotate(90, 0, 0);
            InitializeAudio();
        }

        /// <summary>Attaches an audio source to the camera and starts playing music in a loop.</summary>
        private void InitializeAudio()
        {
            cameraAudioSource = mainCamera.gameObject.AddComponent<AudioSource>();
            cameraAudioSource.loop = true;
            cameraAudioSource.volume = AudioConstants.VOLUME;
            cameraAudioSource.clip = AudioConstants.BACKGROUND_MUSIC;
            cameraAudioSource.Play();
        }

        /// <summary>Centers the camera based on the current dimensions of the maze.</summary>
        public void CenterCamera()
        {
            int mazeWidth = MazeGenerator.GetInstance().width;
            int mazeHeight = MazeGenerator.GetInstance().height;

            //The base values for the camera height
            float camY_widthBase = MazeConstants.WIDTH_MIN * CameraConstants.CAMERA_SCALE_BASE;
            float camY_HeightBase = MazeConstants.HEIGHT_MIN * CameraConstants.CAMERA_SCALE_BASE;

            //Camera height values that change depending on the width/height
            float camY_width = camY_widthBase + (mazeWidth - MazeConstants.WIDTH_MIN) * CameraConstants.CAMERA_SCALE_WIDTH;
            float camY_height = camY_HeightBase + (mazeHeight - MazeConstants.HEIGHT_MIN) * CameraConstants.CAMERA_SCALE_HEIGHT;

            //Choose the max of the two and use it as the camera height
            float camY_max = (camY_width > camY_height) ? camY_width : camY_height;

            //Calculate the X and Z center positions based on the width, height and an X offset
            float xOffset = -camY_max * CameraConstants.CAMERA_SCALE_XOFFSET;
            float camX_center = mazeWidth % 2 == 0 ? xOffset : xOffset + GraphicsConstants.CELL_HALF_SIZE;
            float camZ_center = mazeHeight % 2 == 0 ? 0f : GraphicsConstants.CELL_HALF_SIZE;

            //Set the new center position for the camera, and ser the orthographic size
            Vector3 center = new Vector3(camX_center, camY_max, camZ_center);
            mainCamera.transform.position = center;
            mainCamera.orthographicSize = camY_max * CameraConstants.CAMERA_SCALE_ORTHOGRAPHIC_SIZE;
        }

        /// <summary>Changes camera. Switches between 3 cameras.</summary>
        public void ChangeCamera()
        {
            GameplayHandler.GetInstance().playerGO.GetComponent<InputHandler>().ToggleUIOccupied();
            StartCoroutine(ChangeCameraCoroutine());
        }

        /// <summary>Coroutine for switching between cameras.</summary>
        IEnumerator ChangeCameraCoroutine()
        {
            Camera thirdPersonCamera = CameraController.GetInstance().TP;
            Camera firstPersonCamera = CameraController.GetInstance().FP;
            Camera selfPersonCamera = CameraController.GetInstance().SP;

            if (activeCamera == 0)
            {
                activeCamera = 1;
                mainCamera.targetDisplay = -1;
                thirdPersonCamera.targetDisplay = 0;
                firstPersonCamera.targetDisplay = -1;
                selfPersonCamera.targetDisplay = -1;
                mainCamera.enabled = false;
                thirdPersonCamera.enabled = true;
                firstPersonCamera.enabled = false;
                selfPersonCamera.enabled = false;
            }
            else if (activeCamera == 1)
            {
                activeCamera = 2;
                mainCamera.targetDisplay = -1;
                thirdPersonCamera.targetDisplay = -1;
                firstPersonCamera.targetDisplay = 0;
                selfPersonCamera.targetDisplay = -1;
                mainCamera.enabled = false;
                thirdPersonCamera.enabled = false;
                firstPersonCamera.enabled = true;
                selfPersonCamera.enabled = false;
            }
            else if (activeCamera == 2)
            {
                activeCamera = 0;
                mainCamera.targetDisplay = 0;
                thirdPersonCamera.targetDisplay = -1;
                firstPersonCamera.targetDisplay = -1;
                selfPersonCamera.targetDisplay = -1;
                mainCamera.enabled = true;
                thirdPersonCamera.enabled = false;
                firstPersonCamera.enabled = false;
                selfPersonCamera.enabled = false;
            }
            yield return new WaitForSeconds(0.5f);
            GameplayHandler.GetInstance().playerGO.GetComponent<InputHandler>().ToggleUIOccupied();
        }
    }
}