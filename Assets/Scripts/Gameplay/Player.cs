﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MazeGenerator
{
    //Cardinal Orientation
    using CO = Navigation.CO;

    /// <summary>
    /// The Player class holds the position and orientation of the player.
    /// </summary>
    public class Player : MonoBehaviour
    {
        /// <summary>The player's Animation.</summary>
        public Animation playerAnim;

        /// <summary>The player's cell position.</summary>
        private Cell position;
        /// <summary>The player's orientation.</summary>
        private CO orientation;

        /// <summary>Property for the player's cell position.</summary>
        public Cell Position
        {
            get { return position; }
            set { position = value; }
        }

        /// <summary>Property for the player's orientation.</summary>
        public CO Orientation
        {
            get { return orientation; }
            set { orientation = value; }
        }

        /// <summary>Start is called before the first frame update (MonoBehaviour method). Initializes the player's data.</summary>
        void Start()
        {
            playerAnim = GetComponent<Animation>();
            position = new Cell(MazeGenerator.GetInstance().width - 1, 0);
            orientation = CO.North;
        }
    }
}
