﻿using UnityEngine;

namespace MazeGenerator
{
    /// <summary>
    /// The Orb class represents an orb in the maze. It handles collision events with the player.
    /// </summary>
    public class Orb : MonoBehaviour
    {
        /// <summary>Start is called before the first frame update (MonoBehaviour method).
        /// Starts a loop of animation.</summary>
        void Start()
        {
            MoveUp();
        }

        /// <summary>Called when the player collides with this orb.</summary>
        /// <param name="col">The player's collider.</param>
        void OnTriggerEnter(Collider col)
        {
            GameplayHandler.GetInstance().RemoveOrb(gameObject);
        }

        /// <summary>Starts an animation that moves the orb up.</summary>
        public void MoveUp()
        {
            AnimationHandler.MoveOrbUp(gameObject);
        }

        /// <summary>Starts an animation that moves the orb down.</summary>
        public void MoveDown()
        {
            AnimationHandler.MoveOrbDown(gameObject);
        }
    }
}
