﻿using UnityEngine;

namespace MazeGenerator
{
    /// <summary>Handles all gameplay aspects.</summary>
    public class GameplayHandler
    {
        /// <summary>The instance of GameplayHandler.</summary>
        private static GameplayHandler instance;

        /// <summary>Indicates if the player is able to exit the maze (i.e. all orbs have been collected).</summary>
        private bool canExitMaze;
        /// <summary>The total number of orbs.</summary>
        private int totalOrbs;
        /// <summary>The number of remaining orbs.</summary>
        private int remainingOrbs;

        /// <summary>The player GameObject.</summary>
        public GameObject playerGO;
        /// <summary>The exit gate GameObject.</summary>
        public GameObject exitGateGO;

        /// <summary>Indicates if the player is able to exit the maze (i.e. all orbs have been collected).</summary>
        public bool CanExitMaze
        {
            get { return canExitMaze; }
            set { canExitMaze = value; }
        }

        /// <summary>Returns the total number of orbs.</summary>
        public int TotalOrbs
        {
            get { return totalOrbs; }
        }

        /// <summary>Returns the remaining number of orbs.</summary>
        public int RemainingOrbs
        {
            get { return remainingOrbs; }
        }

        /// <summary>Initializes a new instance of the <see cref="GameplayHandler"/> class.</summary>
        private GameplayHandler()
        {
            InitializeGameplay();
        }

        /// <summary>Gets the only instance of this class.</summary>
        /// <returns>GameplayHandler instance.</returns>
        public static GameplayHandler GetInstance()
        {
            if (instance == null)
            {
                instance = new GameplayHandler();
            }
            return instance;
        }

        /// <summary>Initializes gameplay.</summary>
        private void InitializeGameplay()
        {
            canExitMaze = false;
            totalOrbs = 0;
            remainingOrbs = 0;

            playerGO = new GameObject();
            exitGateGO = new GameObject();
        }

        /// <summary>Removes an orb from the maze.</summary>
        /// <param name="orbGO">The orb GameObject.</param>
        public void RemoveOrb(GameObject orbGO)
        {
            remainingOrbs--;
            if(remainingOrbs==0) AnimationHandler.OpenGate(exitGateGO);

            GraphicsHandler.GetInstance().RemoveGraphicsObject(orbGO);
            UIHandler.GetInstance().UpdateOrbCounter();
        }
        
        /// <summary>Allows the player to cross the exit gate. Is called when the gate animation is completed.</summary>
        public void AllowPassageOutOfMaze()
        {
            canExitMaze = true;
        }

        /// <summary>Starts play mode. Is called when the maze is done generating.</summary>
        public void StartPlayMode()
        {
            //Generate a number of orbs based on the number of cells and a fixed density
            int numberOfCells = MazeGenerator.GetInstance().width * MazeGenerator.GetInstance().height;
            totalOrbs = Mathf.CeilToInt(MazeConstants.ORB_DENSITY * numberOfCells);
            remainingOrbs = totalOrbs;

            //Update UI
            UIHandler.GetInstance().cameraButton.interactable = true;
            UIHandler.GetInstance().UpdateOrbCounter();

            //Create character and orbs graphics
            GraphicsHandler.GetInstance().InstantiateCharacter();
            GraphicsHandler.GetInstance().InstantiateOrbs(totalOrbs);
        }

        /// <summary>Ends play mode. The player wins.</summary>
        public void EndPlayMode()
        {
            //Freeze controls
            playerGO.GetComponent<InputHandler>().ToggleOccupied();

            //Set robot-facing camera to display
            Camera.allCameras[0].targetDisplay = -1;
            Camera.allCameras[0].enabled = false;

            CameraBehaviour.GetInstance().activeCamera = 3;
            CameraController.GetInstance().SP.targetDisplay = 0;
            CameraController.GetInstance().SP.enabled = true;

            //Play the robot's victory animation
            AnimationHandler.PlayVictoryAnimation(playerGO);

            //Show the ending text
            UIHandler.GetInstance().gameCompletePanel.SetActive(true);
        }
    }
}
