﻿using System.Collections.Generic;
using UnityEngine;

namespace MazeGenerator
{
    //Cardinal Orientation
    using CO = Navigation.CO;

    /// <summary>
    /// The PlayerMovement class handles player movement. It allows the player to move or rotate in a certain direction.
    /// </summary>
    public static class PlayerMovement
    {
        /// <summary>
        /// Handles player movement based on the desired direction/orientation.
        /// Movement depends on the player's current orientation.
        /// </summary>
        /// <param name="direction">The target direction.</param>
        public static void HandlePlayerMovement(CO direction)
        {
            GameObject playerGO = GameplayHandler.GetInstance().playerGO;
            CO orientation = playerGO.GetComponent<Player>().Orientation;
            if (orientation == direction || orientation == Navigation.OppositeCO(direction))
            {
                MovePlayerTowards(direction);
            }
            else
            {
                RotatePlayerTowards(direction);
            }
        }

        /// <summary>Moves the player towards the desired direction/orientation.</summary>
        /// <param name="direction">The target direction.</param>
        private static void MovePlayerTowards(CO direction)
        {
            GameObject playerGO = GameplayHandler.GetInstance().playerGO;
            Cell position = playerGO.GetComponent<Player>().Position;
            Cell nextCell = Navigation.NextCell(position, direction);

            //Check if this is the game-ending move
            if (GameplayHandler.GetInstance().CanExitMaze && nextCell.X == 0 && nextCell.Z == MazeGenerator.GetInstance().height)
            {
                GameplayHandler.GetInstance().EndPlayMode();
            }
            //If invalid move, return
            else if (!IsValidPlayerMove(playerGO, nextCell)) return;

            playerGO.GetComponent<InputHandler>().ToggleOccupied();
            playerGO.GetComponent<Player>().Position = nextCell;
            AnimationHandler.MovePlayerTowards(playerGO, direction);
        }

        /// <summary>Rotates the player towards the desired direction/orientation.</summary>
        /// <param name="direction">The target direction.</param>
        private static void RotatePlayerTowards(CO direction)
        {
            GameObject playerGO = GameplayHandler.GetInstance().playerGO;
            AnimationHandler.RotatePlayerTowards(playerGO, direction);
            playerGO.GetComponent<Player>().Orientation = direction;
        }

        /// <summary>
        /// Checks if a player move is valid (if the target cell is within bounds and there's no wall in between).
        /// </summary>
        /// <param name="playerGO">The player GameObject.</param>
        /// <param name="target">The target cell.</param>
        /// <returns><c>true</c> if the player move is valid; otherwise, <c>false</c>.</returns>
        private static bool IsValidPlayerMove(GameObject playerGO, Cell target)
        {
            Cell position = playerGO.GetComponent<Player>().Position;
            HashSet<Wall> mazeWalls = MazeGenerator.GetInstance().GetMazeWalls();
            Wall wallAhead = new Wall(position, target);
            if (mazeWalls.Contains(wallAhead)) return false;
            if (!Navigation.CellWithinBounds(target)) return false;
            return true;
        }
    }
}
