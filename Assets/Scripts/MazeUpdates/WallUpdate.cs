﻿namespace MazeGenerator
{
    /// <summary>
    /// A structure that holds a wall update. It includes a wall and a boolean that says if it should be removed or created.
    /// </summary>
    public struct WallUpdate
    {
        /// <summary>The wall.</summary>
        public Wall wall;
        /// <summary>Whether to remove or create this wall.</summary>
        public bool removeWall;

        /// <summary>
        /// Initializes a new instance of <see cref="WallUpdate"/> class.
        /// </summary>
        public WallUpdate(Wall _wall, bool _removeWall = true) {
            wall = _wall;
            removeWall = _removeWall;
        }
    }
}