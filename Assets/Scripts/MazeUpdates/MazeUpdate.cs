﻿using System.Collections.Generic;

namespace MazeGenerator
{
    /// <summary>
    /// Holds a maze update. It includes a list of WallUpdate and a list of CellUpdate.
    /// </summary>
    public class MazeUpdate
    {
        /// <summary>A list of wall updates.</summary>
        public List<WallUpdate> wallUpdates;
        /// <summary>A list of cell updates.</summary>
        public List<CellUpdate> cellUpdates;

        /// <summary>
        /// Initializes a new instance of <see cref="MazeUpdate"/> class.
        /// </summary>
        public MazeUpdate()
        {
            wallUpdates = new List<WallUpdate>();
            cellUpdates = new List<CellUpdate>();
        }

        /// <summary>Adds a wall as a wall update.</summary>
        /// <param name="wall">The updated wall.</param>
        /// <param name="removeWall">Whether to remove or create this wall.</param>
        public void AddWall(Wall wall, bool removeWall = true)
        {
            WallUpdate wallUpdate = new WallUpdate(wall, removeWall);
            wallUpdates.Add(wallUpdate);
        }

        /// <summary>Adds a cell as a cell update.</summary>
        /// <param name="cell">The updated cell.</param>
        /// <param name="cellType">The new CellType of this cell.</param>
        public void AddCell(Cell cell, CellType cellType)
        {
            CellUpdate cellUpdate = new CellUpdate(cell, cellType);
            cellUpdates.Add(cellUpdate);
        }
    }
}