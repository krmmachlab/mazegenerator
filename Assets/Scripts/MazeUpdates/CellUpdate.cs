﻿namespace MazeGenerator
{
    /// <summary>Cell type.</summary>
    public enum CellType
    {
        /// <summary>Explored but not yet part of the maze.</summary>
        Explored,
        /// <summary>Unexplored.</summary>
        Unexplored,
        /// <summary>Part of the maze.</summary>
        Maze,
        /// <summary>Current lead cell and most recent updated cell.</summary>
        Lead
    }

    /// <summary>
    /// A structure that holds a cell update. It includes a cell and a CellType.
    /// </summary>
    public struct CellUpdate
    {
        /// <summary>The cell.</summary>
        public Cell cell;
        /// <summary>The cell type.</summary>
        public CellType cellType;

        /// <summary>
        /// Initializes a new instance of <see cref="CellUpdate"/> class.
        /// </summary>
        public CellUpdate(Cell _cell, CellType _cellType = CellType.Maze)
        {
            cell = _cell;
            cellType = _cellType;
        }
    }
}