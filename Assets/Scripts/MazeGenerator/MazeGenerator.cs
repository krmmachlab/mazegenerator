﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MazeGenerator
{
    /// <summary>
    /// The MazeGenerator class is responsible for generating a maze.
    /// </summary>
    public class MazeGenerator : MonoBehaviour
    {
        /// <summary>The only instance of MazeGenerator.</summary>
        private static MazeGenerator instance;

        /// <summary>The width of the maze.</summary>
        public int width;
        /// <summary>The height of the maze.</summary>
        public int height;
        /// <summary>The algorithm used to generate the maze.</summary>
        public MazeGenerationAlgorithm mazeGenerationAlgorithm;

        /// <summary>The set of inner maze walls. These are enough to define the maze.</summary>
        private HashSet<Wall> mazeWalls;

        /// <summary>The FPS Text.</summary>
        public Text FPSText;
        /// <summary>Frame count.</summary>
        private int frameCount = 0;
        /// <summary>Next Update.</summary>
        private float nextUpdate = 0.0f;
        /// <summary>Current FPS.</summary>
        public static float fps = 0.0f;
        /// <summary>Rate of updating the FPS.</summary>
        private float updateRate = 4.0f;  // 4 updates per sec.

        /// <summary>
        /// Start is called before the first frame update (MonoBehaviour method). Initializes the MazeGenerator fields.
        /// </summary>
        private void Start()
        {
            Application.targetFrameRate = 60;
            FPSText.text = "0 FPS";
            nextUpdate = Time.time;
            instance = this;
            InitializeFields();
        }

        /// <summary>
        /// Updates the FPS counter.
        /// </summary>
        private void UpdateFPS()
        {
            frameCount++;
            if (Time.time > nextUpdate)
            {
                nextUpdate += 1.0f / updateRate;
                fps = frameCount * updateRate;
                frameCount = 0;
            }
            FPSText.text = fps + " FPS";
        }

        /// <summary>
        /// Update is called at each frame (MonoBehaviour method). Updates the FPS counter.
        /// </summary>
        private void Update()
        {
            UpdateFPS();
        }

        /// <summary>Returns the only instance of MazeGenerator.</summary>
        /// <returns>The MazeGenerator instance.</returns>
        public static MazeGenerator GetInstance()
        {
            return instance;
        }

        /// <summary>Returns the set of inner maze walls.</summary>
        /// <returns>Set of inner maze walls.</returns>
        public HashSet<Wall> GetMazeWalls()
        {
            return mazeWalls;
        }

        /// <summary>Initializes the MazeGenerator fields.</summary>
        private void InitializeFields()
        {
            width = MazeConstants.WIDTH_MIN;
            height = MazeConstants.HEIGHT_MIN;
            mazeGenerationAlgorithm = new MazeGenerationAlgorithm_RecursiveBacktracker(width, height);
            mazeWalls = new HashSet<Wall>();
        }

        /// <summary>Generates a maze.</summary>
        /// <param name="_width">The width of the maze.</param>
        /// <param name="_height">The height of the maze.</param>
        /// <param name="_algorithm">The algorithm that should be used to generate the maze.</param>
        public void Generate(int _width, int _height, int _algorithm)
        {
            //Stop all coroutines that might be running from previous maze generations
            StopAllCoroutines();
            SetData(_width, _height, _algorithm);

            //Display main camera and center it
            Camera.allCameras[0].targetDisplay = -1;
            Camera.allCameras[0].enabled = false;
            CameraBehaviour.GetInstance().mainCamera.targetDisplay = 0;
            CameraBehaviour.GetInstance().mainCamera.enabled = true;
            CameraBehaviour.GetInstance().activeCamera = 0;
            Camera.main.GetComponent<CameraBehaviour>().CenterCamera();

            GameplayHandler.GetInstance().CanExitMaze = false;
            GraphicsHandler.GetInstance().InitializeGraphics();

            StartCoroutine(GenerateMazeCoroutine());
        }

        /// <summary>Sets the maze generator data.</summary>
        /// <param name="_width">The width of the maze.</param>
        /// <param name="_height">The height of the maze.</param>
        /// <param name="_algorithm">The algorithm that should be used to generate the maze.</param>
        private void SetData(int _width, int _height, int _algorithm)
        {
            width = _width;
            height = _height;

            switch (_algorithm)
            {
                case 0:
                    mazeGenerationAlgorithm = new MazeGenerationAlgorithm_RecursiveBacktracker(_width, _height);
                    break;
                case 1:
                    mazeGenerationAlgorithm = new MazeGenerationAlgorithm_Kruskal(_width, _height);
                    break;
                case 2:
                    mazeGenerationAlgorithm = new MazeGenerationAlgorithm_Prim(_width, _height);
                    break;
                case 3:
                    mazeGenerationAlgorithm = new MazeGenerationAlgorithm_Wilson(_width, _height);
                    break;
                case 4:
                    mazeGenerationAlgorithm = new MazeGenerationAlgorithm_RecursiveDivision(_width, _height);
                    break;
            }
        }

        /// <summary>The generate maze coroutine.</summary>
        IEnumerator GenerateMazeCoroutine()
        {
            while (!mazeGenerationAlgorithm.MazeComplete)
            {
                MazeUpdate mazeUpdates = mazeGenerationAlgorithm.ExecuteStep();
                GraphicsHandler.GetInstance().UpdateMaze(mazeUpdates);
                yield return null;
            }
            CompleteGeneration();
        }

        /// <summary>Called after the maze is done generating. Starts play mode.</summary>
        private void CompleteGeneration()
        {
            mazeWalls = GraphicsHandler.GetInstance().GetWalls();
            GameplayHandler.GetInstance().StartPlayMode();
        }
    }
}