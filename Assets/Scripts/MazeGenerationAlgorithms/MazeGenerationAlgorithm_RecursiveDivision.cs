﻿using System.Collections.Generic;

namespace MazeGenerator
{
    //Since a wall is defined by 2 cells, we can use it as a partition (which is also defined by 2 cells)
    using Partition = Wall;

    /// <summary>
    /// Implements the Recursive Division algorithm for generating a maze.
    /// This algorithm is recursive, but since we execute algorithm steps one at a time, 
    /// I used a stack to simulate recursiveness (instead of implementing normal recursion).
    /// The algorithm details can be seen on https://en.wikipedia.org/wiki/Maze_generation_algorithm
    /// </summary>
    /// <seealso cref="MazeGenerationAlgorithm" />
    public class MazeGenerationAlgorithm_RecursiveDivision : MazeGenerationAlgorithm
    {
        /// <summary>A stack of pending partitions. A partition is defined by 2 corner cells.</summary>
        private Stack<Partition> partitionStack;

        /// <summary>Initializes a new instance of the <see cref="MazeGenerationAlgorithm_RecursiveDivision"/> class.</summary>
        public MazeGenerationAlgorithm_RecursiveDivision(int _width, int _height) : base(_width, _height)
        {
            InitializeAlgorithm();
        }

        /// <summary>Initializes the algorithm by pushing the entire maze to the stack as the first partition.</summary>
        private void InitializeAlgorithm()
        {
            partitionStack = new Stack<Partition>();
            Cell bottomLeftCell = new Cell(0, 0);
            Cell topRightCell = new Cell(width - 1, height - 1);

            partitionStack.Push(new Partition(bottomLeftCell, topRightCell));
        }

        /// <summary>Executes a step of the maze generation algorithm.</summary>
        /// <returns>MazeUpdate that includes wall and cell updates.</returns>
        public override MazeUpdate ExecuteStep()
        {
            MazeUpdate mazeUpdate = new MazeUpdate();

            //If the stack is empty, the maze is complete
            if (partitionStack.Count == 0)
            {
                mazeComplete = true;
                return mazeUpdate;
            }
            Partition partition = partitionStack.Peek();
            //If the partition has a width or height of 1, return
            if (partition.Cell1.X == partition.Cell2.X || partition.Cell1.Z == partition.Cell2.Z)
            {
                partitionStack.Pop();
                return mazeUpdate;
            }
            DividePartition(ref mazeUpdate);
            return mazeUpdate;
        }

        /// <summary>
        /// Divides the current parent partition with 4 walls (and 3 holes), then pushes the 4 smaller partitions to the stack.
        /// </summary>
        /// <param name="mazeUpdate">Reference to the MazeUpdate.</param>
        private void DividePartition(ref MazeUpdate mazeUpdate)
        {
            Partition parentPartition = partitionStack.Peek();
            //Decide the x and z cuts randomly
            int xCut = rnd.Next(parentPartition.Cell1.X, parentPartition.Cell2.X);
            int zCut = rnd.Next(parentPartition.Cell1.Z, parentPartition.Cell2.Z);

            CutPartitionWithWalls(xCut, zCut, ref mazeUpdate);

            Partition bottomLeftPartition = new Partition(new Cell(parentPartition.Cell1.X, parentPartition.Cell1.Z), new Cell(xCut, zCut));
            Partition bottomRightPartition = new Partition(new Cell(xCut + 1, parentPartition.Cell1.Z), new Cell(parentPartition.Cell2.X, zCut));
            Partition topLeftPartition = new Partition(new Cell(parentPartition.Cell1.X, zCut + 1), new Cell(xCut, parentPartition.Cell2.Z));
            Partition topRightPartition = new Partition(new Cell(xCut + 1, zCut + 1), new Cell(parentPartition.Cell2.X, parentPartition.Cell2.Z));

            partitionStack.Pop();
            partitionStack.Push(bottomLeftPartition);
            partitionStack.Push(bottomRightPartition);
            partitionStack.Push(topLeftPartition);
            partitionStack.Push(topRightPartition);
        }

        /// <summary>Cuts the current parent partition with 4 walls.</summary>
        /// <param name="xCut">The x-axis cut.</param>
        /// <param name="zCut">The z-axis cut.</param>
        /// <param name="mazeUpdate">Reference to the MazeUpdate.</param>
        private void CutPartitionWithWalls(int xCut, int zCut, ref MazeUpdate mazeUpdate)
        {
            //Choose one of the 4 walls to exclude from having a hole
            int excludedCut = rnd.Next(4);
            HorizontalCut_Left(xCut, zCut, excludedCut, ref mazeUpdate);
            HorizontalCut_Right(xCut, zCut, excludedCut, ref mazeUpdate);
            VerticalCut_Bottom(xCut, zCut, excludedCut, ref mazeUpdate);
            VerticalCut_Top(xCut, zCut, excludedCut, ref mazeUpdate);
        }

        /// <summary>Cuts the current parent partition horizontally from the left.</summary>
        /// <param name="xCut">The x-axis cut.</param>
        /// <param name="zCut">The z-axis cut.</param>
        /// <param name="excludedCut">The index of the cut to exclude from having a hole.</param>
        /// <param name="mazeUpdate">Reference to the MazeUpdate.</param>
        private void HorizontalCut_Left(int xCut, int zCut, int excludedCut, ref MazeUpdate mazeUpdate)
        {
            Partition parentPartition = partitionStack.Peek();
            int randomHole = rnd.Next(parentPartition.Cell1.X, xCut + 1);
            for (int x = parentPartition.Cell1.X; x <= xCut; x++)
            {
                if (excludedCut != 0 && x == randomHole)
                {
                    continue;
                }
                Cell cell1 = new Cell(x, zCut);
                Cell cell2 = new Cell(x, zCut + 1);
                Wall divisionWall = new Wall(cell1, cell2);
                mazeUpdate.AddWall(divisionWall, false);
            }
        }

        /// <summary>Cuts the current parent partition horizontally from the right.</summary>
        /// <param name="xCut">The x-axis cut.</param>
        /// <param name="zCut">The z-axis cut.</param>
        /// <param name="excludedCut">The index of the cut to exclude from having a hole.</param>
        /// <param name="mazeUpdate">Reference to the MazeUpdate.</param>
        private void HorizontalCut_Right(int xCut, int zCut, int excludedCut, ref MazeUpdate mazeUpdate)
        {
            Partition parentPartition = partitionStack.Peek();
            int randomHole = rnd.Next(xCut + 1, parentPartition.Cell2.X + 1);
            for (int x = xCut + 1; x <= parentPartition.Cell2.X; x++)
            {
                if (excludedCut != 1 && x == randomHole)
                {
                    continue;
                }
                Cell cell1 = new Cell(x, zCut);
                Cell cell2 = new Cell(x, zCut + 1);
                Wall divisionWall = new Wall(cell1, cell2);
                mazeUpdate.AddWall(divisionWall, false);
            }
        }

        /// <summary>Cuts the current parent partition vertically from the bottom.</summary>
        /// <param name="xCut">The x-axis cut.</param>
        /// <param name="zCut">The z-axis cut.</param>
        /// <param name="excludedCut">The index of the cut to exclude from having a hole.</param>
        /// <param name="mazeUpdate">Reference to the MazeUpdate.</param>
        private void VerticalCut_Bottom(int xCut, int zCut, int excludedCut, ref MazeUpdate mazeUpdate)
        {
            Partition parentPartition = partitionStack.Peek();
            int randomHole = rnd.Next(parentPartition.Cell1.Z, zCut + 1);
            for (int z = parentPartition.Cell1.Z; z <= zCut; z++)
            {
                if (excludedCut != 2 && z == randomHole)
                {
                    continue;
                }
                Cell cell1 = new Cell(xCut, z);
                Cell cell2 = new Cell(xCut + 1, z);
                Wall divisionWall = new Wall(cell1, cell2);
                mazeUpdate.AddWall(divisionWall, false);
            }
        }

        /// <summary>Cuts the current parent partition vertically from the top.</summary>
        /// <param name="xCut">The x-axis cut.</param>
        /// <param name="zCut">The z-axis cut.</param>
        /// <param name="excludedCut">The index of the cut to exclude from having a hole.</param>
        /// <param name="mazeUpdate">Reference to the MazeUpdate.</param>
        private void VerticalCut_Top(int xCut, int zCut, int excludedCut, ref MazeUpdate mazeUpdate)
        {
            Partition parentPartition = partitionStack.Peek();
            int randomHole = rnd.Next(zCut + 1, parentPartition.Cell2.Z + 1);
            for (int z = zCut + 1; z <= parentPartition.Cell2.Z; z++)
            {
                if (excludedCut != 3 && z == randomHole)
                {
                    continue;
                }
                Cell cell1 = new Cell(xCut, z);
                Cell cell2 = new Cell(xCut + 1, z);
                Wall divisionWall = new Wall(cell1, cell2);
                mazeUpdate.AddWall(divisionWall, false);
            }
        }
    }
}