﻿using System.Collections.Generic;

namespace MazeGenerator
{
    /// <summary>
    /// Implements the Recursive Backtracker algorithm for generating a maze.
    /// The algorithm details can be seen on https://en.wikipedia.org/wiki/Maze_generation_algorithm
    /// </summary>
    /// <seealso cref="MazeGenerationAlgorithm" />
    public class MazeGenerationAlgorithm_RecursiveBacktracker : MazeGenerationAlgorithm
    {
        /// <summary>A stack holding all cells that are being explored.</summary>
        private Stack<Cell> cellStack;
        /// <summary>A set that includes all visited cells.</summary>
        private HashSet<Cell> visited;
        /// <summary>The cell that was the lead in the previous step.</summary>
        private Cell lastLead;
        /// <summary>The type of the last lead.</summary>
        private CellType lastLeadType;

        /// <summary>Initializes a new instance of the <see cref="MazeGenerationAlgorithm_RecursiveBacktracker"/> class.</summary>
        public MazeGenerationAlgorithm_RecursiveBacktracker(int _width, int _height) : base(_width, _height)
        {
            InitializeAlgorithm();
        }

        /// <summary>Initializes the algorithm by choosing a random cell, marking it as visited, and pushing it to the stack.</summary>
        private void InitializeAlgorithm()
        {
            cellStack = new Stack<Cell>();
            visited = new HashSet<Cell>();
            Cell startingCell = new Cell(rnd.Next(width), rnd.Next(height));
            visited.Add(startingCell);
            cellStack.Push(startingCell);
            lastLead = startingCell;
            lastLeadType = CellType.Explored;
        }

        /// <summary>Executes a step of the maze generation algorithm.</summary>
        /// <returns>MazeUpdate that includes wall and cell updates.</returns>
        public override MazeUpdate ExecuteStep()
        {
            MazeUpdate mazeUpdate = new MazeUpdate();
            mazeUpdate.AddCell(lastLead, lastLeadType);

            //If the stack is empty, the maze is complete
            if (cellStack.Count == 0)
            {
                mazeComplete = true;
                return mazeUpdate;
            }
            Cell currentCell = cellStack.Peek();

            //Get the current cell's unvisited neighbors
            List<Cell> unvisitedNeighbors = GetUnvisitedNeighbors(currentCell);
            if(unvisitedNeighbors.Count == 0)
            {
                //If it has no unvisited neighbors, pop it
                PopCell(ref mazeUpdate);
            }
            else
            {
                int neighborIndex = rnd.Next(unvisitedNeighbors.Count);
                Cell randomNeighbor = unvisitedNeighbors[neighborIndex];
                //Break the wall between the current cell and its selected neighbor, and push the neighbor to the stack
                BreakWallAndPush(currentCell, randomNeighbor, ref mazeUpdate);
            }
            return mazeUpdate;
        }

        /// <summary>Returns a list of all unvisited neighbors of the passed cell.</summary>
        /// <param name="cell">The cell.</param>
        /// <returns>List of all unvisited neighbors.</returns>
        private List<Cell> GetUnvisitedNeighbors(Cell cell)
        {
            List<Cell> unvisitedNeighbors = new List<Cell>();
            foreach(Cell neighbor in cell.CalculateNeighbors())
            {
                if (!visited.Contains(neighbor)) unvisitedNeighbors.Add(neighbor);
            }
            return unvisitedNeighbors;
        }

        /// <summary>Pops the last cell from the stack and updates the mazeUpdate.</summary>
        /// <param name="mazeUpdate">Reference to the MazeUpdate.</param>
        private void PopCell(ref MazeUpdate mazeUpdate)
        {
            Cell poppedCell = cellStack.Pop();
            mazeUpdate.AddCell(poppedCell, CellType.Maze);
            lastLead = poppedCell;
            lastLeadType = CellType.Maze;
        }

        /// <summary>
        /// Breaks the wall between the current cell and its selected neighbor, and pushes the neighbor to the stack.
        /// It also updates the mazeUpdate.
        /// </summary>
        /// <param name="currentCell">The current cell.</param>
        /// <param name="randomNeighbor">A random neighbor of the current cell.</param>
        /// <param name="mazeUpdate">Reference to the MazeUpdate.</param>
        private void BreakWallAndPush(Cell currentCell, Cell randomNeighbor, ref MazeUpdate mazeUpdate)
        {
            Wall wall = new Wall(currentCell, randomNeighbor);
            visited.Add(randomNeighbor);
            cellStack.Push(randomNeighbor);
            mazeUpdate.AddWall(wall);
            mazeUpdate.AddCell(randomNeighbor, CellType.Lead);
            lastLead = randomNeighbor;
            lastLeadType = CellType.Explored;
        }
    }
}