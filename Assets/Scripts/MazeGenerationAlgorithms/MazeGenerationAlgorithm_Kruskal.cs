﻿using System.Collections.Generic;
using UnityEngine;

namespace MazeGenerator
{
    /// <summary>
    /// Implements Kruskal's algorithm for generating a maze.
    /// The algorithm details can be seen on https://en.wikipedia.org/wiki/Maze_generation_algorithm
    /// </summary>
    /// <seealso cref="MazeGenerationAlgorithm" />
    public class MazeGenerationAlgorithm_Kruskal : MazeGenerationAlgorithm
    {
        /// <summary>The index of the current wall in the list of walls.</summary>
        private int currentWallIndex;
        /// <summary>A list of all inner maze walls.</summary>
        private List<Wall> walls;
        /// <summary>A dictionary that returns the set that a cell belongs to.</summary>
        private Dictionary<Cell, HashSet<Cell>> cellToSet;

        /// <summary>Initializes a new instance of the <see cref="MazeGenerationAlgorithm_Kruskal"/> class.</summary>
        public MazeGenerationAlgorithm_Kruskal(int _width, int _height) : base(_width, _height)
        {
            InitializeWalls();
            InitializeSets();
        }

        /// <summary>Adds all walls to the wall list and shuffles it.</summary>
        private void InitializeWalls()
        {
            currentWallIndex = 0;
            walls = new List<Wall>();

            AddHorizontalWalls();
            AddVerticalWalls();
            walls.Shuffle();
        }

        /// <summary>Initializes the set of each cell. Each cell starts in its own set.</summary>
        private void InitializeSets()
        {
            cellToSet = new Dictionary<Cell, HashSet<Cell>>();
            for (int x = 0; x < width; x++)
            {
                for (int z = 0; z < height; z++)
                {
                    Cell cell = new Cell(x, z);
                    HashSet<Cell> set = new HashSet<Cell>();
                    set.Add(cell);
                    cellToSet.Add(cell, set);
                }
            }
        }

        /// <summary>Adds all horizontal walls to the wall list.</summary>
        private void AddHorizontalWalls()
        {
            for (int x = 0; x < width; x++)
            {
                for (int z = 0; z < height - 1; z++)
                {
                    Cell cell1 = new Cell(x, z);
                    Cell cell2 = new Cell(x, z + 1);
                    walls.Add(new Wall(cell1, cell2));
                }
            }
        }

        /// <summary>Adds all vertical walls to the wall list.</summary>
        private void AddVerticalWalls()
        {
            for (int z = 0; z < height; z++)
            {
                for (int x = 0; x < width - 1; x++)
                {
                    Cell cell1 = new Cell(x, z);
                    Cell cell2 = new Cell(x + 1, z);
                    walls.Add(new Wall(cell1, cell2));
                }
            }
        }

        /// <summary>Executes a step of the maze generation algorithm.</summary>
        /// <returns>MazeUpdate that includes wall and cell updates.</returns>
        public override MazeUpdate ExecuteStep()
        {
            MazeUpdate mazeUpdate = new MazeUpdate();

            //If we went through all walls in the list, the maze is complete
            if (currentWallIndex>=walls.Count)
            {
                mazeComplete = true;
                return mazeUpdate;
            }

            Wall currentWall = walls[currentWallIndex];
            if (CurrentWallHasCellsInDistinctSets())
            {
                MergeCellSetsOfCurrentWall();
                mazeUpdate.AddWall(currentWall);
            }

            currentWallIndex++;
            return mazeUpdate;
        }

        /// <summary>Checks if the two cells of the current wall belong to distinct sets.</summary>
        /// <returns><c>true</c> if the two cells of the current wall belong to distinct sets; otherwise, <c>false</c>.</returns>
        private bool CurrentWallHasCellsInDistinctSets()
        {
            Wall currentWall = walls[currentWallIndex];
            Cell cell1 = currentWall.Cell1;
            Cell cell2 = currentWall.Cell2;

            GuardAgainstSetlessCell(cell1);
            GuardAgainstSetlessCell(cell2);
            return (cellToSet[cell1] != cellToSet[cell2]);
        }

        /// <summary>Merges the two distinct sets that the cells of the current wall belong to.</summary>
        private void MergeCellSetsOfCurrentWall()
        {
            Wall currentWall = walls[currentWallIndex];
            Cell cell1 = currentWall.Cell1;
            Cell cell2 = currentWall.Cell2;

            GuardAgainstSetlessCell(cell1);
            GuardAgainstSetlessCell(cell2);

            //Move every cell in set2 to set1
            foreach (Cell cell in cellToSet[cell2])
            {
                cellToSet[cell1].Add(cell);
                cellToSet[cell] = cellToSet[cell1];
            }
        }

        /// <summary>Checks if a cell belongs to a set. Throws an error if the cell doesn't belong to any set.</summary>
        /// <param name="cell">The cell.</param>
        private void GuardAgainstSetlessCell(Cell cell)
        {
            HashSet<Cell> set;
            if (!cellToSet.TryGetValue(cell, out set))
            {
                Debug.LogError("Cell " + cell + " does not belong to any set.");
            }
        }
    }
}