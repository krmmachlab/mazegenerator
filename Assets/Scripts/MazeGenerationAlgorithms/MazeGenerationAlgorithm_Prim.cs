﻿using System.Collections.Generic;
using UnityEngine;

namespace MazeGenerator
{
    /// <summary>
    /// Implements Prim's algorithm for generating a maze.
    /// The algorithm details can be seen on https://en.wikipedia.org/wiki/Maze_generation_algorithm
    /// </summary>
    /// <seealso cref="MazeGenerationAlgorithm" />
    public class MazeGenerationAlgorithm_Prim : MazeGenerationAlgorithm
    {
        /// <summary>A list of all walls pending to be explored.</summary>
        private List<Wall> pendingWalls;
        /// <summary>A set that includes all visited cells.</summary>
        private HashSet<Cell> visited;
        /// <summary>The cell that was the lead in the previous step.</summary>
        private Cell lastLead;

        /// <summary>Initializes a new instance of the <see cref="MazeGenerationAlgorithm_Prim"/> class.</summary>
        public MazeGenerationAlgorithm_Prim(int _width, int _height) : base(_width, _height)
        {
            InitializeAlgorithm();
        }

        /// <summary>Initializes the algorithm by choosing a random cell and adding its walls to the list of pending walls.</summary>
        private void InitializeAlgorithm()
        {
            pendingWalls = new List<Wall>();
            visited = new HashSet<Cell>();

            //Get a random starting cell. Add its walls to pendingWalls
            Cell startingCell = new Cell(rnd.Next(width), rnd.Next(height));
            visited.Add(startingCell);
            foreach (Wall wall in startingCell.CalculateWalls())
            {
                pendingWalls.Add(wall);
            }
            lastLead = startingCell;
        }

        /// <summary>Executes a step of the maze generation algorithm.</summary>
        /// <returns>MazeUpdate that includes wall and cell updates.</returns>
        public override MazeUpdate ExecuteStep()
        {
            MazeUpdate mazeUpdate = new MazeUpdate();

            //Set the previous lead cell type to a normal maze cell
            mazeUpdate.AddCell(lastLead, CellType.Maze);

            //If there are no more pending walls, the maze is complete
            if (pendingWalls.Count == 0)
            {
                mazeComplete = true;
                return mazeUpdate;
            }

            //Get a random wall from the pending walls
            int randomIndex = rnd.Next(pendingWalls.Count);
            Wall currentWall = pendingWalls[randomIndex];

            if(ContainsUnvisitedCell(currentWall))
            {
                AddWallsOfUnvisitedCellToPending(currentWall);
                mazeUpdate.AddCell(lastLead, CellType.Lead);
                mazeUpdate.AddWall(currentWall);
            }
            else
            {
                //If the current wall has no unvisited cells, remove it
                pendingWalls.Remove(currentWall);
            }

            return mazeUpdate;
        }

        /// <summary>Checks if the passed wall contains an unvisited cell.</summary>
        /// <param name="wall">The wall.</param>
        /// <returns><c>true</c> if the passed wall contains an unvisited cell; otherwise, <c>false</c>.</returns>
        private bool ContainsUnvisitedCell(Wall wall)
        {
            return !visited.Contains(wall.Cell1) || !visited.Contains(wall.Cell2);
        }

        /// <summary>Returns the unvisited cell of the passed wall.</summary>
        /// <param name="wall">The wall.</param>
        /// <returns>The unvisited cell. If none exists, an error is thrown.</returns>
        private Cell GetUnvisitedCell(Wall wall)
        {
            if (!visited.Contains(wall.Cell1)) return wall.Cell1;
            if (!visited.Contains(wall.Cell2)) return wall.Cell2;
            Debug.LogError("Wall " + wall + " does not have an unvisited cell.");
            return new Cell(-1, -1);
        }

        /// <summary>Adds the walls of the unvisited cell of the current wall to the list of pending walls.</summary>
        /// <param name="currentWall">The current wall.</param>
        private void AddWallsOfUnvisitedCellToPending(Wall currentWall)
        {
            Cell unvisitedCell = GetUnvisitedCell(currentWall);
            visited.Add(unvisitedCell);
            foreach (Wall wall in unvisitedCell.CalculateWalls())
            {
                //Don't add the current wall again
                if (wall == currentWall) continue;
                pendingWalls.Add(wall);
            }
            lastLead = unvisitedCell;
        }
    }
}