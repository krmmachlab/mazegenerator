﻿namespace MazeGenerator
{
    /// <summary>MazeGenerationAlgorithm is the abstract class for a maze generation algorithm.
    /// It returns a MazeUpdate whenever ExecuteStep() is called.</summary>
    public abstract class MazeGenerationAlgorithm
    {
        /// <summary>Random number generator.</summary>
        protected static System.Random rnd = new System.Random();

        /// <summary>Maze width.</summary>
        protected int width;
        /// <summary>Maze height.</summary>
        protected int height;
        /// <summary>Indicates if the maze generation is complete.</summary>
        protected bool mazeComplete;

        /// <returns>Returns <c>true</c> if the maze is complete.</returns>
        public bool MazeComplete
        {
            get { return mazeComplete; }
        }

        /// <summary>Initializes a new instance of the <see cref="MazeGenerationAlgorithm"/> class.</summary>
        protected MazeGenerationAlgorithm(int _width, int _height)
        {
            width = _width;
            height = _height;
            mazeComplete = false;
        }

        /// <summary>Executes a step of the maze generation algorithm.</summary>
        /// <returns>MazeUpdate that includes wall and cell updates.</returns>
        public abstract MazeUpdate ExecuteStep();
    }
}