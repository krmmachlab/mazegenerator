﻿using System.Collections.Generic;
using System.Linq;

namespace MazeGenerator
{
    /// <summary>
    /// Implements Wilson's algorithm for generating a maze.
    /// The algorithm details can be seen on https://en.wikipedia.org/wiki/Maze_generation_algorithm
    /// and http://weblog.jamisbuck.org/2011/1/20/maze-generation-wilson-s-algorithm
    /// </summary>
    /// <seealso cref="MazeGenerationAlgorithm" />
    public class MazeGenerationAlgorithm_Wilson : MazeGenerationAlgorithm
    {
        /// <summary>Generation state of the algorithm.</summary>
        private enum GenerationState
        {
            /// <summary>A random walk has just been traced, now back to idle.</summary>
            Idle,
            /// <summary>The algorithm is performing a random walk.</summary>
            RandomWalk,
            /// <summary>The algorithm is tracing the path of the random walk.</summary>
            TracePath
        }

        /// <summary>The set of cells that are part of the maze.</summary>
        private HashSet<Cell> mazeCells;
        /// <summary>The set of unvisited cells.</summary>
        private HashSet<Cell> unvisitedCells;
        /// <summary>The set of cells that are part of the current random walk.</summary>
        private HashSet<Cell> randomWalkCells;
        /// <summary>A dictionary that maps a cell to the direction of the next cell. Used in tracing the random walk.</summary>
        private Dictionary<Cell, Cell> cellDirectionOffset;
        /// <summary>The current state of Wilson's algorithm.</summary>
        private GenerationState generationState;
        /// <summary>The starting cell of the current random walk.</summary>
        private Cell randomWalkStartingCell;
        /// <summary>The current cell of the current random walk.</summary>
        private Cell randomWalkCurrentCell;

        /// <summary>Initializes a new instance of the <see cref="MazeGenerationAlgorithm_Wilson"/> class.</summary>
        public MazeGenerationAlgorithm_Wilson(int _width, int _height) : base(_width, _height)
        {
            InitializeFields();
            InitializeAlgorithm();
        }

        /// <summary>Initializes the fields of this algorithm.</summary>
        private void InitializeFields()
        {
            mazeCells = new HashSet<Cell>();
            unvisitedCells = new HashSet<Cell>();
            randomWalkCells = new HashSet<Cell>();
            cellDirectionOffset = new Dictionary<Cell, Cell>();
            generationState = GenerationState.Idle;
        }

        /// <summary>
        /// Initializes the the algorithm by setting all cells to unvisited and choosing a random cell as part of the maze.
        /// </summary>
        private void InitializeAlgorithm()
        {
            for (int x = 0; x < width; x++)
            {
                for (int z = 0; z < height; z++)
                {
                    Cell cell = new Cell(x, z);
                    unvisitedCells.Add(cell);
                }
            }

            //Choose a random cell and make it part of the maze
            Cell startingCell = new Cell(rnd.Next(width), rnd.Next(height));
            unvisitedCells.Remove(startingCell);
            mazeCells.Add(startingCell);
            randomWalkCurrentCell = startingCell;
        }

        /// <summary>Executes a step of the maze generation algorithm.</summary>
        /// <returns>MazeUpdate that includes wall and cell updates.</returns>
        public override MazeUpdate ExecuteStep()
        {
            MazeUpdate mazeUpdate = new MazeUpdate();

            //If there are no more unvisited cells, the maze is complete
            if(unvisitedCells.Count == 0)
            {
                mazeComplete = true;
                return mazeUpdate;
            }
            //Choose which action to perform based on the current state
            switch(generationState)
            {
                case GenerationState.Idle:
                    mazeUpdate = InitializeRandomWalk();
                    break;
                case GenerationState.RandomWalk:
                    mazeUpdate = PerformRandomWalk();
                    break;
                case GenerationState.TracePath:
                    mazeUpdate = TracePath();
                    break;
            }
            return mazeUpdate;
        }

        /// <summary>Initializes a random walk and chooses a random cell outside the maze.</summary>
        /// <returns>MazeUpdate that includes wall and cell updates.</returns>
        private MazeUpdate InitializeRandomWalk()
        {
            MazeUpdate mazeUpdate = new MazeUpdate();
            mazeUpdate.AddCell(randomWalkCurrentCell, CellType.Maze);
            randomWalkCurrentCell = randomWalkStartingCell = GetRandomUnvisitedCell();
            generationState = GenerationState.RandomWalk;
            cellDirectionOffset.Clear();
            randomWalkCells.Clear();
            mazeUpdate.AddCell(randomWalkCurrentCell, CellType.Lead);
            return mazeUpdate;
        }

        /// <summary>Performs a step a the random walk.</summary>
        /// <returns>MazeUpdate that includes wall and cell updates.</returns>
        private MazeUpdate PerformRandomWalk()
        {
            MazeUpdate mazeUpdate = new MazeUpdate();
            WalkToRandomNeighbor(ref mazeUpdate);

            //Found a maze cell, so switch to next state
            if (mazeCells.Contains(randomWalkCurrentCell))
            {
                randomWalkCurrentCell = randomWalkStartingCell;
                generationState = GenerationState.TracePath;
            }
            mazeUpdate.AddCell(randomWalkCurrentCell, CellType.Lead);
            return mazeUpdate;
        }

        /// <summary>Performs a step of tracing the path of the random walk.</summary>
        /// <returns>MazeUpdate that includes wall and cell updates.</returns>
        private MazeUpdate TracePath()
        {
            MazeUpdate mazeUpdate = new MazeUpdate();
            unvisitedCells.Remove(randomWalkCurrentCell);
            mazeCells.Add(randomWalkCurrentCell);
            mazeUpdate.AddCell(randomWalkCurrentCell, CellType.Maze);
            mazeUpdate.AddWall(new Wall(randomWalkCurrentCell, randomWalkCurrentCell + cellDirectionOffset[randomWalkCurrentCell]));
            randomWalkCurrentCell += cellDirectionOffset[randomWalkCurrentCell];
            //Found a maze cell, so switch to next state
            if (mazeCells.Contains(randomWalkCurrentCell))
            {
                NeutralizeUntracedCellsInRandomWalk(ref mazeUpdate);
                generationState = GenerationState.Idle;
            }
            return mazeUpdate;
        }

        /// <summary>Returns a random unvisited cell.</summary>
        /// <returns>A random unvisited cell.</returns>
        private Cell GetRandomUnvisitedCell()
        {
            Cell randomCell = unvisitedCells.ElementAt(rnd.Next(unvisitedCells.Count));
            return randomCell;
        }

        /// <summary>Returns a random neighbor of the current random walk cell.</summary>
        /// <returns>Random neighbor of the current random walk cell.</returns>
        private Cell GetRandomNeighbor()
        {
            List<Cell> neighbors = randomWalkCurrentCell.CalculateNeighbors();
            return neighbors[rnd.Next(neighbors.Count)];
        }

        /// <summary>Performs a walk to a random neighbor.</summary>
        /// <param name="mazeUpdate">Reference to the MazeUpdate.</param>
        private void WalkToRandomNeighbor(ref MazeUpdate mazeUpdate)
        {
            Cell randomNeighbor = GetRandomNeighbor();

            //The direction offset is the offset needed to get from the current cell to the neighbor
            Cell directionOffset = randomNeighbor - randomWalkCurrentCell;
            cellDirectionOffset[randomWalkCurrentCell] = directionOffset;
            if (!randomWalkCells.Contains(randomWalkCurrentCell))
                randomWalkCells.Add(randomWalkCurrentCell);
            mazeUpdate.AddCell(randomWalkCurrentCell, CellType.Explored);
            randomWalkCurrentCell = randomNeighbor;
        }

        /// <summary>
        /// Neutralizes all cells of the random walk that were untraced.
        /// Used for graphically coloring the untraced cells back to unexplored color.
        /// </summary>
        /// <param name="mazeUpdate">Reference to the MazeUpdate.</param>
        private void NeutralizeUntracedCellsInRandomWalk(ref MazeUpdate mazeUpdate)
        {
            foreach(Cell cell in randomWalkCells)
            {
                if(!mazeCells.Contains(cell))
                {
                    mazeUpdate.AddCell(cell, CellType.Unexplored);
                }
            }
        }
    }
}