﻿namespace MazeGenerator
{
    /// <summary>A static class that holds the constant values used for the controls.</summary>
    public static class ControlConstants
    {
        // UP
        /// <summary>The first control for Up.</summary>
        public static string CONTROLS_UP1 = "w";
        /// <summary>The second control for Up.</summary>
        public static string CONTROLS_UP2 = "up";

        // DOWN
        /// <summary>The first control for Down.</summary>
        public static string CONTROLS_DOWN1 = "s";
        /// <summary>The second control for Down.</summary>
        public static string CONTROLS_DOWN2 = "down";

        // LEFT
        /// <summary>The first control for Left.</summary>
        public static string CONTROLS_LEFT1 = "a";
        /// <summary>The second control for Left.</summary>
        public static string CONTROLS_LEFT2 = "left";

        // RIGHT
        /// <summary>The first control for Right.</summary>
        public static string CONTROLS_RIGHT1 = "d";
        /// <summary>The second control for Right.</summary>
        public static string CONTROLS_RIGHT2 = "right";

        /// <summary>Hiding UI control.</summary>
        public static string CONTROLS_HIDEUI = "h";

        /// <summary>Changing camera control.</summary>
        public static string CONTROLS_CAMERA = "f";
    }
}