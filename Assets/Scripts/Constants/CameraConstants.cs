﻿namespace MazeGenerator
{
    /// <summary>A static class that holds the constant values used for the camera.</summary>
    public static class CameraConstants
    {
        /// <summary>The scale for the base component of the camera Y value.</summary>
        public static float CAMERA_SCALE_BASE = 1.3f;
        /// <summary>The scale for the width component of the camera Y value.</summary>
        public static float CAMERA_SCALE_WIDTH = 0.7f;
        /// <summary>The scale for the height component of the camera Y value.</summary>
        public static float CAMERA_SCALE_HEIGHT = 0.9f;
        /// <summary>The scale for the X offset of the camera.</summary>
        public static float CAMERA_SCALE_XOFFSET = 0.27f;
        /// <summary>The scale for orthographic size of the camera.</summary>
        public static float CAMERA_SCALE_ORTHOGRAPHIC_SIZE = 0.6f;
    }
}