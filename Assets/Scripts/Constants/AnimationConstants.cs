﻿using UnityEngine;

namespace MazeGenerator
{
    /// <summary>A static class that holds the constant values used for animations.</summary>
    public static class AnimationConstants
    {
        /// <summary>The default Animations path.</summary>
        public static string ANIMATION_PATH = "Animations/";
        /// <summary>The victory animation clip.</summary>
        public static AnimationClip ANIMATION_VICTORY = (AnimationClip)Resources.Load(ANIMATION_PATH + "VictoryAnimation");

        /// <summary>The time for the player animation.</summary>
        public static float PLAYER_ANIMATION_TIME = 0.3f;
        /// <summary>The time for the orb animation.</summary>
        public static float ORB_ANIMATION_TIME = 1f;
        /// <summary>The time for the gate animation.</summary>
        public static float GATE_ANIMATION_TIME = 1f;
        /// <summary>The time for the orb progress bar animation.</summary>
        public static float BAR_ANIMATION_TIME = 1f;
        /// <summary>The gate's end position when it slides down.</summary>
        public static float GATE_ANIMATION_END_POSITION = -0.6f;
        /// <summary>How much an orb moves up and down from its original position.</summary>
        public static float ORB_ANIMATION_DISTANCE = 0.1f;
    }
}