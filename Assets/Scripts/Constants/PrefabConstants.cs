﻿namespace MazeGenerator
{
    /// <summary>A static class that holds the constant values used for prefabs.</summary>
    public static class PrefabConstants
    {
        /// <summary>The default Prefabs path.</summary>
        public static string PREFAB_PATH = "Prefabs/";
        /// <summary>The Prefab path for the character.</summary>
        public static string PREFAB_CHARACTER = PREFAB_PATH + "Robot";
        /// <summary>The Prefab path for the maze cell.</summary>
        public static string PREFAB_CELL = PREFAB_PATH + "Cell";
        /// <summary>The Prefab path for the maze pillar.</summary>
        public static string PREFAB_PILLAR = PREFAB_PATH + "Pillar";
        /// <summary>The Prefab path for the maze wall.</summary>
        public static string PREFAB_WALL = PREFAB_PATH + "Wall";
        /// <summary>The Prefab path for the maze gate.</summary>
        public static string PREFAB_GATE = PREFAB_PATH + "Gate";
        /// <summary>The Prefab path for the orb.</summary>
        public static string PREFAB_ORB = PREFAB_PATH + "Orb";
    }
}