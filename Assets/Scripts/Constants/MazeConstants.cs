﻿namespace MazeGenerator
{
    /// <summary>
    ///   <para>
    ///     This project implements a maze generator. The UI allows you to choose a width, height and algorithm
    ///     for generating the maze. You can click 'Generate Maze' to start the generation. You can see the maze
    ///     being generating with live updates each frame. At any time during the maze generation, you can interrupt
    ///     it and generate a new maze. I implemented 5 different algorithms for generating a maze.
    ///     The project is meant to be played on a 1920x1080 resolution and does not scale properly with other
    ///     resolutions.
    ///   </para>  
    ///   <para>
    ///     Gameplay:
    ///     After a maze is generated, the application turns into a game. You control a robot that needs to collect
    ///     all orbs in the maze in order to open the exit gate. When the gate opens, you can leave the maze and win!
    ///     There is a progress bar in the UI that tells you how many orbs you've collected and how many remain.
    ///     You can change camera and switch between orthographic top-down view, third-person view, and first-person view.
    ///     Top-down view allows you to see the entire maze so you know where you need to go. The other 2 views make
    ///     the game harder and more challenging, as you can only see what's in front of you. If you choose to play
    ///     in third- or first-person view, the spikes on the pillars are a good indicator of how deep you are in the maze.
    ///   </para>
    ///   <para>
    ///     Assets:
    ///     I modeled all assets myself using Autodesk Maya: The robot, wall, and pillar. The cell, gate and orb are simple
    ///     objects.
    ///     The music used is royalty-free. Source:
    ///     https://youtu.be/DzYp5uqixz0?list=PLfP6i5T0-DkJPT4dkMAr0PwRq1m25UtoO
    ///   </para>
    ///   <para>
    ///     Animation:
    ///     I made use of the LeanTween animation asset for most simple animations. For the robot victory animation,
    ///     I used the ‘Animation’ window in Unity to create an animation clip.
    ///   </para>
    /// </summary>
    internal class NamespaceDoc
    {
    }

    /// <summary>A static class that holds the constant values used for the maze.</summary>
    public static class MazeConstants
    {/// <summary>The minimum allowed maze width.</summary>
        public static int WIDTH_MIN = 3;
        /// <summary>The maximum allowed maze width.</summary>
        public static int WIDTH_MAX = 60; //A value above 60 creates problems with LeanTween
        /// <summary>The minimum allowed maze height.</summary>
        public static int HEIGHT_MIN = 3;
        /// <summary>The maximum allowed maze height.</summary>
        public static int HEIGHT_MAX = 60; //A value above 60 creates problems with LeanTween
        /// <summary>The orb density.</summary>
        public static float ORB_DENSITY = 0.1f;
    }
}