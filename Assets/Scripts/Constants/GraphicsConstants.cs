﻿using UnityEngine;

namespace MazeGenerator
{
    /// <summary>A static class that holds the constant values used for graphics.</summary>
    public static class GraphicsConstants
    {
        /// <summary>The size of a cell GameObject.</summary>
        public static float CELL_SIZE = 1f;
        /// <summary>Half the size of a cell GameObject.</summary>
        public static float CELL_HALF_SIZE = CELL_SIZE * 0.5f;

        /// <summary>The height at which cells are instantiated.</summary>
        public static float CELL_ELEVATION = -0.1f;
        /// <summary>The height at which pillars are instantiated.</summary>
        public static float PILLAR_ELEVATION = 0.3f;
        /// <summary>The height at which walls are instantiated.</summary>
        public static float WALL_ELEVATION = 0.23f;
        /// <summary>The height at which gates are instantiated.</summary>
        public static float GATE_ELEVATION = 0.5f;
        /// <summary>The height at which the character is instantiated.</summary>
        public static float CHARACTER_ELEVATION = 0.4f;
        /// <summary>The height at which orbs are instantiated.</summary>
        public static float ORB_ELEVATION = 0.4f;

        /// <summary>The character scale.</summary>
        public static float CHARACTER_SCALE = 1.1f;

        /// <summary>The color of a cell that's part of the maze.</summary>
        public static Color MAZE_CELL_COLOR = Color.black;
        /// <summary>The color of an explored cell.</summary>
        public static Color EXPLORED_CELL_COLOR = Color.yellow;
        /// <summary>The color of an unexplored cell.</summary>
        public static Color UNEXPLORED_CELL_COLOR = Color.grey;
        /// <summary>The color of a lead cell.</summary>
        public static Color LEAD_CELL_COLOR = Color.red;
    }
}