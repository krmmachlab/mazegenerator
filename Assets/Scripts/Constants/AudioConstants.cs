﻿using UnityEngine;

namespace MazeGenerator
{
    /// <summary>A static class that holds the constant values used for audio.</summary>
    public static class AudioConstants
    {
        /// <summary>The default audio path.</summary>
        public static string AUDIO_PATH = "Audio/";
        /// <summary>Background music audio clip.</summary>
        public static AudioClip BACKGROUND_MUSIC = (AudioClip)Resources.Load(AUDIO_PATH + "BackgroundMusic");
        /// <summary>Music volume.</summary>
        public static float VOLUME = 0.1f;
    }
}