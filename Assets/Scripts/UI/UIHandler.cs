﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace MazeGenerator
{
    /// <summary>
    /// The UIHandler class handles everything UI related.
    /// </summary>
    public class UIHandler : MonoBehaviour
    {
        /// <summary>The only instance of UIHandler.</summary>
        private static UIHandler instance;

        /// <summary>The width slider.</summary>
        public Slider widthSlider;
        /// <summary>The height slider.</summary>
        public Slider heightSlider;
        /// <summary>The text for the width value.</summary>
        public Text widthValueText;
        /// <summary>The text for the height value.</summary>
        public Text heightValueText;
        /// <summary>The Dropdown for the generation algorithm.</summary>
        public Dropdown algorithmDropdown;
        /// <summary>The button for generating the maze.</summary>
        public Button generateButton;
        /// <summary>The button for changing camera.</summary>
        public Button cameraButton;
        /// <summary>The progress bar for orbs collected.</summary>
        public GameObject orbBar;
        /// <summary>The text for the orb counter.</summary>
        public Text orbCounterText;
        /// <summary>The Panel for the text that appears when you finish the game.</summary>
        public GameObject gameCompletePanel;

        /// <summary>The current width value on the slider.</summary>
        private int widthSliderValue = MazeConstants.WIDTH_MIN;
        /// <summary>The current height value on the slider.</summary>
        private int heightSliderValue = MazeConstants.HEIGHT_MIN;

        /// <summary>Status of UI visibility.</summary>
        private bool showUI;

        /// <summary>Start is called before the first frame update (MonoBehaviour method). Initializes the UI for the scene.</summary>
        void Start()
        {
            instance = this;
            InitializeUI();
        }

        /// <summary>Returns the only instance of UIHandler.</summary>
        /// <returns>The UIHandler instance.</returns>
        public static UIHandler GetInstance()
        {
            return instance;
        }

        /// <summary>Initializes the UI for the scene.</summary>
        private void InitializeUI()
        {
            showUI = true;
            cameraButton.interactable = false;
            widthValueText.text = (MazeConstants.WIDTH_MIN).ToString();
            heightValueText.text = (MazeConstants.HEIGHT_MIN).ToString();

            //Add listeners
            widthSlider.onValueChanged.AddListener(delegate { OnWidthInputChange(); });
            heightSlider.onValueChanged.AddListener(delegate { OnHeightInputChange(); });
            generateButton.onClick.AddListener(delegate () { GenerateMaze(); });
            cameraButton.onClick.AddListener(delegate () { CameraBehaviour.GetInstance().ChangeCamera(); });
        }

        /// <summary>Toggles the UI between visible and hidden.</summary>
        public void ToggleUI()
        {
            GameplayHandler.GetInstance().playerGO.GetComponent<InputHandler>().ToggleUIOccupied();
            StartCoroutine(ToggleUICoroutine());
        }

        /// <summary>Coroutine for toggling the UI.</summary>
        IEnumerator ToggleUICoroutine()
        {
            showUI = !showUI;
            foreach(Transform child in transform)
            {
                GameObject childGO = child.gameObject;
                if (childGO == gameCompletePanel) continue;
                if (childGO == MazeGenerator.GetInstance().FPSText.gameObject) continue;
                if (childGO.tag == "progress")
                {
                    Vector3 pos = childGO.transform.position;
                    float factor = showUI ? 1.0f : -1.0f;
                    factor *= (Screen.height*380/1080);
                    childGO.transform.position = new Vector3(pos.x, pos.y + factor, pos.z);
                    continue;
                }

                childGO.SetActive(showUI);
            }
            yield return new WaitForSeconds(0.5f);
            GameplayHandler.GetInstance().playerGO.GetComponent<InputHandler>().ToggleUIOccupied();
        }

        /// <summary>Starts generating the maze. Gets called when the generate button is clicked.</summary>
        private void GenerateMaze()
        {
            ResetUI();
            MazeGenerator.GetInstance().Generate(widthSliderValue, heightSliderValue, algorithmDropdown.value);
        }

        /// <summary>Resets the UI.</summary>
        private void ResetUI()
        {
            cameraButton.interactable = false;
            gameCompletePanel.SetActive(false);
            AnimationHandler.ScaleBar(orbBar, 0f);
        }

        /// <summary>Updates the current width value. Called whenever the value of the width slider changes.</summary>
        private void OnWidthInputChange()
        {
            widthSliderValue = Mathf.FloorToInt(MazeConstants.WIDTH_MIN + (widthSlider.value * (MazeConstants.WIDTH_MAX - MazeConstants.WIDTH_MIN)));
            widthValueText.text = widthSliderValue.ToString();
        }

        /// <summary>Updates the current height value. Called whenever the value of the height slider changes.</summary>
        private void OnHeightInputChange()
        {
            heightSliderValue = Mathf.FloorToInt(MazeConstants.HEIGHT_MIN + (heightSlider.value * (MazeConstants.HEIGHT_MAX - MazeConstants.HEIGHT_MIN)));
            heightValueText.text = heightSliderValue.ToString();
        }
        
        /// <summary>Updates the orb counter Text and its corresponding progress bar.</summary>
        public void UpdateOrbCounter()
        {
            int totalOrbs = GameplayHandler.GetInstance().TotalOrbs;
            int remainingOrbs = GameplayHandler.GetInstance().RemainingOrbs;
            float progress = (totalOrbs - remainingOrbs) / (1f * totalOrbs);
            AnimationHandler.ScaleBar(orbBar, progress);
            orbCounterText.text = (totalOrbs - remainingOrbs).ToString() + " / " + totalOrbs.ToString();
        }
    }
}